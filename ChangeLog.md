# DDMoRe Repository Change Log #

## Version 1.1.0 ##

**Date 17 Apr 2018**

### Enhancements ###

* Reorganised submission guidelines.  A common complaint among users was you couldn't see the
submission guidelines to know what you needed for submission until you had logged in and actually
 initiated the submission process.  Now we have reorganised the the guidelines so thay can be viewed
  from the home page as well as during submission.  They are more of a help page now.
  
* Google Analytics support.  The repository now links to Google Analytics which should 
    provide us with more usage information. 



## Version 1.0.2 ##

**Date: 13 Nov 2017**

## Bug Fixes ##

* Issue #21:  Variable definitions in the observations model cause a crash and are are not rendered. 

## Version 1.0.1 ##

**Date: 22 Jun 2017**

## Bug Fixes ##

* Issue #14: There was a problem where information about recently accessed models
was overlapping with the table of models when the browser page size was sufficiently small.
The recently accessed models list has been removed and this fixes the problem.  

## Version 1.0.0 ##

**Date: 22 May 2017**

### Deployment ###

 

Note the following are not strictly changes to the code, but to the configuration of the deployment.

* Previously all users had the curation role set and new users were assigned this role by default.  This has been changed.
    Now no users have this role and new users will not have this role assigned to them.  Clearly not all users are curators
     in the sense that they are there to assess models from other users.  In addition there is a performance bug associated with this.
     If you publish a model for review, then previously all users were assigned this model for review.  This task took a very long time
     to execute (>5 mins).  By reducing the number of curators we avoid this problem.  The bug is Issue #6.

### Enhancements ###

* Issue #10: In the model display page the submission ID is now included in the title.  Previously there was not way to get this ID
 unless you went back to the Browse models page.

### Bug fixes ###

* Issue #1: In the PharmML 0.8.1 display page, equations were very long and tended to extend beyond the page boundary.
            To address this, the MathML generation is now removes unnecessary parenthesis which makes equations much more
            concise and removes this problem in all cases checked.  Of course for very long equations the problem will
            persist.

* Issue #3: PharmML 0.8.1 models could not be annotated and therefore published.  This has been fixed.

* Issue #4: When annotating the PharmML file it is modified.  In the newly generated PharmML the namespace prefixes where
            unhelpfully named.  This has been fixed.

* Issue #5: When displaying target mapping in a data file (mapping to PK Macros) the presentation was confusing and also
            there was a bug that meant it couldn't handle cases where the mapping was conditional on a data value.
            This bug is resolved and now the target mapping is presented in the same way as other data mappings, which
            should be much clearer.

* Issues #7: When a user updated the notificiation properties in their profile this failed with an error.  This has been
             fixed.

