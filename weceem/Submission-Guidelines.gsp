        <h1> Submission Guidelines</h1>
        <div> Before you start submission, it is recommended that you browse the current Repository
            and get familiar with how it is organised. Try to find a similar model before you
            prepare your material to be uploaded.</div>
        <div> </div>
        <div> The following submission guidelines have been defined to guide you through the
            necessary steps (minimal requirements) you have to follow to bring your model into the
            public domain with high quality information. Please read them carefully.Please Note: If
            you encounter issues preventing you from publishing your model in the DDMoRe Model
            Repository, you can share your model with the user called <span
                style="font-family:arial,helvetica,sans-serif;">Help</span> (click on your model
            entry then on the share button on the left). In doing so, you will benefit from their
            expertise and receive hints on how to improve your model's compliance with the minimal
            requirements.</div>
        <h3> Ground rules for submission</h3>
        <ul>
            <li> The quality of the information depends solely on submitters.</li>
            <li> Any model can be shared to the public if fulfilling minimum requirements (described
                in the next steps).</li>
            <li> If a model refers to a publication, it has to have the same or equivalent
                mathematical representation. If this is not the case, the publication can still be
                referred to but modifications to the publication and their motivations needs to be
                clearly detailed in the documents uploaded.</li>
            <li> Uploading of observed human data is not accepted. This might be changed in the
                future as the project evolves.</li>
            <li> A simulated data set is needed to let other users: <ul>
                    <li> understand how the data set has to be constructed, i.e. which data items,
                        specific records, etc., are needed;</li>
                    <li> run the model, e.g. execute estimation/simulation OR execute an evaluation
                        (MAX=0 in NONMEM): the type of execution is the submitter's decision.</li>
                </ul>
            </li>
        </ul>
        <p> Therefore, a data set with the dependent variable simulated from the uploaded model OR
            with a dummy dependent variable is acceptable.</p>
        <ul>
            <li> Output files are needed to <ul>
                    <li> prove the model is executable (output from execution of simulated data is
                        sufficient for this purpose);</li>
                    <li> report the final model parameter estimates and corresponding uncertainty
                        (output from execution of real data is needed for this purpose).</li>
                </ul>
            </li>
        </ul>
        <p> When real data are accessible to the submitter, the output file obtained from the real
            data needs to be provided. When real data are not available, an output file containing
            the information about parameter estimates and their uncertainty, as available in the
            publication, needs to be provided.</p>
        <h3> Create relevant login credentials</h3>
        <p> If you do not have an account on the DDMoRe Repository, please register and you will be
            sent a password that will allow you to log on and start the submission process. Your
            name will be displayed as submitter for the models you upload, thus define it
            appropriately: first name, space, last name (you can also modify it once logged in by
            selecting `Edit User' from your profile page).</p>
        <h3> Identify the scenario</h3>
        <p> To allow as many models as possible to be uploaded, a number of submission scenarios
            have been identified based on the possibility to generate MDL and/or PharmML code. The
            documentation required to upload a model varies based on the scenario chosen. Therefore,
            please use the decision tree below to identify which case is applicable to your model.
            For scenarios 1, 2 and 4, example models are given below, which may be helpful.Once that
            is done, follow carefully the instructions below for the chosen scenario, all steps are
            mandatory if not differently specified.</p>
        <figure style="display: table; text-align: center; margin: 10px auto;">
            <img src="http://localhost:8080/model-repository/images/ddmore/decisionTree.png"
                style="margin-left: auto; margin-right: auto; display: block" />
            <figcaption style="display: table-caption; caption-side: bottom; text-align: left"
                        ><span><sup style="font-weight: bold">a</sup></span>
                <span style="font-style: italic; font-weight: bold"> The option to upload a model
                    with reduced functionality is provided to cater for situations where the MDL
                    and/or PharmML language versions supportd by the Respository do not support all
                    the features of the original model. However, a companion file named
                    Model_Accommodations.txt needs to clearly state how the model was modified in
                    order to implement it in MDL and PharmML.</span>
                <div style="font-style: italic; font-weight: bold">
                    <sup>b</sup>The option to upload MDL and PharmML codes which are not executable
                    is provided to cater for situations where the automatic converters from these
                    languages to target tools languages are not aligned with the most recent
                    developments of MDL and PharmML. Modifications to the code produced by automatic
                    conversion from MDL or PharmML need to be described in the companion named
                    Command.txt.</div>
            </figcaption></figure>
        <div>
            <p> In the text for each scenario below, the minimal requirements for publication are
                underlined.</p>
        </div>
        <h3> Follow the file naming rules</h3>
        <p> Before preparing the material to be uploaded to the DDMoRe Repository, please look at
            the naming rules below, covering the files to be uploaded <a href="#namingConvention"
                title="Click here to jump to the naming convention for all uploaded files"
            >below</a>.</p>
        <p> Go to <a href="#scenario1">scenario 1</a> | <a href="#scenario2">scenario 2</a> | <a
                href="#scenario3">scenario 3</a> | <a href="#scenario4">scenario 4</a></p>



        <h4> Scenario 1: Executable MDL AND PharmML code</h4>
        <p> This applied when the original model can be successfully coded in both MDL and PharmML
            and that the MDL and PharmML code can be executed in the Interoperability Framework.</p>
        <p>
            <strong>Example model:</strong> Friberg_2009_Schizophrenia_Asenapine_PANSS [<a
                href="http://repository.ddmore.foundation/model/DDMODEL00000002" target="_blank"
                >http://repository.ddmore.foundation/model/DDMODEL00000002</a>]</p>
        <ol>
            <li> Start submission by uploading <span class="underlined">PharmML code (version up to
                    0.6.1) as the mandatory file</span> (the DDMoRe Repository will thus provide an
                automatic human-friendly model visualization within the Model Definition tab).</li>
            <li> Upload the following additional files. Add a short and meaningful description in
                the box next to each file. <ol type="a">
                    <li>
                        <span class="underlined">MDL code (version 7.0 or later supported by DDMoRe
                            Interoperability Framework 4.1 or later)</span>.</li>
                    <li>
                        <span class="underlined">Real data (possible if non-human) or simulated
                            data</span>. If simulated: provide a dataset similar to the real dataset
                        with dependent variable(s) simulated from the uploaded model OR with dummy
                        dependent variable(s), and with covariates replaced by their typical
                        values.</li>
                    <li>
                        <span class="underlined">Command.txt</span>, which describes the successful
                        execution command/script: this file also needs to specify the uploaded file
                        names (for input data, executable model and output results of the execution)
                        and the used language/product versions/target tool, see <a
                            href="#uploadCmd1">example 1</a>.</li>
                    <li>
                        <span class="underlined">Output file</span> obtained after execution of
                        uploaded dataset and MDL model code.</li>
                    <li>
                        <span class="underlined">Additional output file (if you decided to upload
                            simulated data)</span>, with the final model parameter estimates and the
                        corresponding uncertainty. If you have access to real data and do not upload
                        them, this is the output file obtained after execution with real data. If
                        you do not have access to real data but the model refers to a publication,
                        this file should include final estimates and corresponding uncertainty as
                        given in the publication used as reference, see <a href="#outputExample"
                            >output_example</a>.</li>
                    <li> Model_Accommodations.txt if you had to modify the published model: describe
                        in this file the differences between the implemented model and the model in
                        the publication used as reference. Use <a href="#modelAccommodations">this
                            template</a> and see also examples in the following two model
                        entries:<br /> Hamren_2008_diabetes_tesaglitazar [<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000003"
                            target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000003</a>],<br />
                        Trefz_2015_metabolism_Kuvan_TurnoverKPD [<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000103"
                            target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000103</a>].</li>
                    <li> Any other files considered to be useful.</li>
                </ol>
            </li>
            <li> After pressing Upload, fill in Model Information.<br /> In the box
                    <strong>Name</strong>: change the default to a describion of your model (max 150
                characters). This is the descriptive name which will be shown when browsing the
                DDMoRe Model Repository (see [<a href="http://repository.ddmore.foundation/models"
                    target="_blank">http://repository.ddmore.foundation/models</a>] for
                examples).<br />
            </li>
            <li> In the box <strong>Description</strong>: add a brief and meaningful description of
                your model. This text is shown in the Overview tab, but not directly in the browser
                of the DDMoRe Model Repository.</li>
            <li> Next add a reference for your model. If the model is related to a publication,
                provide the corresponding publication identifier: PubMed ID (preferred), DOI, other
                link (URL) or choose &quot;Publication without link&quot;.</li>
            <li> Before pressing &quot;Complete Submission&quot;, check the &quot;Summary of your
                submission&quot; (click on &quot;Back&quot; within the repository and refine if
                necessary).</li>
            <li> After completing the submission, enter the model annotations by pressing the button
                &quot;Annotate&quot; (see buttons on the left-had bar) and fill in the fields on all
                available tabs, thenpress &quot;Save&quot; to save the information you provided and
                &quot;Validate&quot; to check the validity of the annotations you entered.</li>
            <li>
                <span class="underlined">Click on &quot;Return to model display page&quot; and then
                    on the &quot;Publish&quot; button (see the buttons on the left-hand bar) in
                    order to share your model with the community and complete the publishing
                    process. Before publishing, please ensure you provided all the files and
                    information described in previous steps, otherwise the (automatic) publishing
                    process will reject the model publication. If you need to modify your model to
                    have it published, click on the button &quot;Update&quot; on the left-hand
                    bar.</span></li>
        </ol>




        <h4> Scenario 2: MDL and PharmML codes and executable code</h4>
        <p> This scenario applies when the MDL and PharmML code cannot be entirely executed in the
            DDMoRe Interoperability Framework (via translation of MDL and PharmML to the target tool
            language) and an additional executable code (e.g. NMTRAN, MLXTRAN) must be provided.</p>
        <p>
            <strong>Example model:</strong> Hamren_2008_diabetes_tesaglitazar [<a
                href="http://repository.ddmore.foundation/model/DDMODEL00000003" target="_blank"
                >http://repository.ddmore.foundation/model/DDMODEL00000003</a>]</p>
        <ol>
            <li> Start submission by uploading a non-executable <span class="underlined">PharmML
                    code (version up to 0.6.1) as mandatory file</span> (the Model Repository will
                thus provide an automatic human-friendly model visualization within the Model
                Definition tab).</li>
            <li> Upload the following as additional files. Add a short and meaningful description in
                the box next to each file. <ol type="a">
                    <li>
                        <span class="underlined">A non-executable MDL code (version 7.0 or later
                            supported by DDMoRe Interoperability Framework 4.1)</span>.</li>
                    <li>
                        <span class="underlined">An executable code (e.g. NMTRAN, MLXTRAN). The
                            submitter is encouraged to use the MDL or PharmML code, convert it into
                            the target tool language usingthe DDMoRe Interoperability Framework, and
                            modify the produced code in order to make it executable.</span></li>
                    <li>
                        <span class="underlined">Real data (possible if non-human) or simulated
                            data</span>. If simulated: provide a dataset similar to the real dataset
                        with dependent variable(s) simulated from the uploaded model OR with dummy
                        dependent variable(s), and with covariates replaced by their typical
                        values.</li>
                    <li>
                        <span class="underlined">Command.txt</span>, which describes the successful
                        execution command/script: this file also needs to specify the uploaded file
                        names (for input data, executable model, and output results of the
                        execution) and the used language/product versions/target tool. Moreover, the
                        modifications mentioned in point b) above need to be described within this
                        file: as an example, see Hamren_2008_diabetes_tesaglitazar [<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000003"
                            target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000003</a>].</li>
                    <li>
                        <span class="underlined">Output file obtained after execution of uploaded
                            dataset and executable code.</span></li>
                    <li>
                        <span class="underlined">Additional output file if you decided to upload
                            simulated data</span>, with the final model parameter estimates and the
                        corresponding uncertainty. If you have access to real data this is the
                        output file obtained after execution with real data. If you do not have
                        access to real data and do not upload them, this file should include final
                        estimates and corresponding uncertainty as given in the publication used as
                        reference, see <a href="#outputExample">output_example</a>.</li>
                    <li> Model_Accommodations.txt if you had to modify the published model: describe in this
                        file the differences between the implemented model and the model in the publication
                        used as reference (any changes that were required in the executable code to make it
                        run should not be stated here but in the Command.txt file). Use <a
                            href="#modelAccommodations" target="_blank">this template</a> and see also
                        examples in the following two model entries:<br />
                        Hamren_2008_diabetes_tesaglitazar[<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000003" target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000003</a>] and<br />
                        Trefz_2015_metabolism_Kuvan_TurnoverKPD [<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000103" target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000103</a>].</li>
                    <li> Any other files considered to be useful.</li>
                </ol>
            </li>
            <li> After pressing Upload, fill in Model Information.<br /> In the box
                    <strong>Name</strong>: change the default to a description of your model (max
                150 characters). This text is the descriptive name which will be shown when browsing the DDMoRe
                Model Repository (see <a href="http://repository.ddmore.foundation/models" target="_blank"
                    title="existing models on the DDMoRe Repository -- opens in a new window"
                    >http://repository.ddmore.foundation/models</a> for examples).</li>
            <li>In the box <strong>Description</strong>: add a brief and meaningful description of your
                model. This text is shown in the Overview tab, but not directly in the browser of the DDMoRe Model Repository.
                Next add a citation for your model.  If the model is related to a publication,
                provide the corresponding publication identifier: PubMed ID (to be preferred), DOI,
                other link(URL) or choose &quot;Publication without link&quot;.</li>
            <li> Before pressing &quot;Complete Submission&quot;, check the &quot;Summary of your
                submission&quot; (click on &quot;Back&quot; within the DDMoRe Model Repository and refine
                if necessary).</li>
            <li> After completing the submission, enter the model annotations by pressing the button
                &quot;Annotate&quot; (see the buttons on the left-hand bar) and fill in the fields
                on the all available tabs; then press &quot;Save&quot; to save the information you
                provided and &quot;Validate&quot; to check the validity of the annotations you
                entered. If the annotations are correct a message turns up on top stating ‘Annotations are correct’.
            </li>
            <li>
                Click on &quot;return to model display page&quot; and then
                    click on the &quot;Publish&quot; button (see buttons on the left-hand bar) in
                    order to share your model with the community and complete the publishing
                    process. Before publishing, please ensure you provided all the files and
                    information described in the previous steps otherwise the (automatic) publishing
                    process will reject the publication of the model. If you need to modify your
                    model to have it published, click on the &quot;Update&quot; button in the
                    left-hand bar. </li>
        </ol>




        <h4> Scenario 3: MDL and PharmML codes, not intended for execution</h4>
        <p> This scenario applies when the model is not intended to be executed in the
            Interoperability Framework.</p>
        <p>
            <strong>Example model:</strong> None currently</p>
        <ol>
            <li> Start submission by uploading a non-executable <span class="underlined">PharmML
                    code (version up to 0.8.1) as mandatory file</span> (the DDMoRe Model Repository will
                thus provide an automatic human-friendly model visualization within the Model
                Definition tab).</li>
            <li> Upload the following additional files. Add a short and meaningful description in
                the box next to each file. <ol type="a">
                    <li>
                        <span class="underlined">MDL code (version 7.0 or later supported by DDMoRe
                            Interoperability Framework 4.1 or later)</span>.</li>
                    <li> Model_Accommodations.txt if you had to modify the published model: describe
                        in this file the differences between the implemented model and the model in
                        the publication used as reference. Use <a href="#modelAccommodations"
                            target="_blank">this template</a> and see also examples in the following
                        two model entries:<br /> Hamren_2008_diabetes_tesaglitazar [<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000003"
                            target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000003</a>]
                        and<br /> Trefz_2015_metabolism_Kuvan_TurnoverKPD [<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000103"
                            target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000103</a>].</li>
                    <li> Any other files considered to be useful.</li>
                </ol>
            </li>
            <li> After pressing Upload, fill in Model Information.<br /> In the box
                    <strong>Name</strong>: change the default to a description of your model (max
                150 characters). This is the name which will be shown when browsing the DDMoRe Model Repository
                (see <a href="http://repository.ddmore.foundation/models" target="_blank"
                    title="existing models on the DDMoRe Repository -- opens in a new window"
                    >http://repository.ddmore.foundation/models</a> for examples).<br /> In the box
                    <strong>Description</strong>: add a brief and meaningful description of your
                model. This text is shown in the Overview tab, but not directly in the browser of the DDMoRe Model Repository.</li>
            <li> Next add a reference for your model. If the model is related to a publication,
                provide the corresponding publication identifier: PubMed ID (to be preferred), DOI,
                other link(URL) or choose &quot;Publication without link&quot;.</li>
            <li> Before pressing &quot;Complete Submission&quot;, check the &quot;Summary of your
                submission&quot; (click on &quot;Back&quot; within the DDMoRe Model Repository and refine if
                necessary).</li>
            <li> After completing the submission, enter the model annotations by pressing the button
                &quot;Annotate&quot; (see the buttons on the left-hand bar) and fill in the fields
                on the all available tabs; then press &quot;Save&quot; to save the information you
                provided and &quot;Validate&quot; to check the validity of the annotations you
                entered. If annotations are correct a message turns up on top stating ‘Annotations are correct’.</li>
            <li>
                Click on &quot;Return to model display page&quot; and then
                    on the &quot;Publish&quot; button (see the buttons on the left-hand bar) in
                    order to share your model with the community and complete the publishing
                    process. Before publishing, please ensure you provided all the files and
                    information described in the previous steps, otherwise the (automatic)
                    publishing process will reject the model publication. If you need to modify your
                    model to have it published, click on the &quot;Update&quot; button in the
                    left-hand bar.</li>
        </ol>


        <h4> Scenario 4: Executable original code</h4>
        <p> This scenario applies when the model submitter wants to publish a model that for various
            reasons has not been coded in MDL and PharmML.</p>
        <p>
            <strong>Example model:</strong> CMS_Colistin_PK_Karaiskos_2015 [<a
                href="http://repository.ddmore.foundation/model/DDMODEL00000130" target="_blank"
                >http://repository.ddmore.foundation/model/DDMODEL00000130</a>]</p>
        <ol>
            <li> Start submission by uploading <span class="underlined">executable original code as
                    the mandatory file (thus, the DDMoRe Model Repository will NOT be able to provide an
                    automatic human-friendly visualisation)</span>.</li>
            <li> Upload the following as additional files. Add a short and meaningful description in
                the box next to each file. <ol type="a">
                    <li>
                        <span class="underlined">Real data (possible if non-human) or simulated
                            data</span> (if simulated: provide a dataset similar to the real dataset
                        with dependent variable(s) simulated from the uploaded model OR with dummy
                        dependent variable(s), and with covariates replaced by their typical
                        values.</li>
                    <li>
                        <span class="underlined">Command.txt</span>, which describes the successful
                        execution command/script: this file also needs to specify the uploaded file
                        names (for input data, executable model, and output results of the
                        execution) and the used target tool, see <a href="#uploadCmd2">example
                        2</a>.</li>
                    <li>
                        <span class="underlined">Output file</span> obtained after execution of
                        uploaded dataset and executable original model code (it is possible to use
                        equivalent to MAX=0 from NONMEM to save time if simulated dataset is
                        used).</li>
                    <li>
                        <span class="underlined">Additional output file if you decided to upload
                            simulated data</span>, with the final model parameter estimates and the
                        corresponding uncertainty. If you have access to real data this is the
                        output file(s) obtained after execution with real data. If you do not have
                        access to real data, this file should include final estimates and
                        corresponding uncertainty as given in the publication used as reference, see
                            <a href="#outputExample">output_example</a>.</li>
                    <li> Model_Accommodations.txt if you had to modify the published model: describe
                        in this file the differences between the implemented model and the model in
                        the publication used as reference. Use <a href="#modelAccommodations">this
                            template</a> and see also examples in the following two model
                        entries:<br /> Hamren_2008_diabetes_tesaglitazar [<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000003"
                            target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000003</a>],<br />
                        Trefz_2015_metabolism_Kuvan_TurnoverKPD [<a
                            href="http://repository.ddmore.foundation/model/DDMODEL00000103"
                            target="_blank"
                            >http://repository.ddmore.foundation/model/DDMODEL00000103</a>].</li>                    <li> Any other files considered to be useful</li>
                </ol>
            </li>
            <li> After pressing Upload, fill in Model Information.<br /> In the box
                    <strong>Name</strong>: change the default to a description of your model (max
                150 characters). This is the descriptive name which will be shown when browsing the repository
                (see <a href="http://repository.ddmore.foundation/models" target="_blank"
                    title="existing models on the DDMoRe Repository -- opens in a new window"
                    >http://repository.ddmore.foundation/models</a> for examples).<br /> In the box
                    <strong>Description</strong>: add a brief and meaningful description of your
                model. This text is shown in the Overview tab, but not directly in the browser of the DDMoRe Model Repository.</li>
            <li> Next add a reference for your model. If the model is related to a publication,
                provide the corresponding publication identifier: PubMed ID(to be preferred), DOI,
                other link(URL) or choose &quot;Publication without link&quot;.</li>
            <li> Before pressing &quot;Complete Submission&quot;, check the &quot;Summary of your
                submission&quot; (click on &quot;Back&quot; within the DDMoRe Model Repository and refine if
                necessary).</li>
            <li> After completing the submission, enter the model annotations by pressing the button
                &quot;Annotate&quot; (See buttons on the left-hand bar) and fill in the fields on
                the all available tabs; then press &quot;Save&quot; to save the information you
                provided and &quot;Validate&quot; to check the validity of the annotations you
                entered. If annotations are correct a message turns up on top stating ‘Annotations are correct’. </li>
            <li>
                Click on &quot;Return to model display page&quot; and then
                    on &quot;Publish&quot; (see the buttons on the left-hand bar) in order to share
                    your model with the community and to complete the publishing process. Before
                    publishing, please ensure you provided all the files and information described
                    in the previous steps, otherwise the (automatic) publishing process will not
                    publish your model. If you need to modify your model to have it published, click
                    on the&quot;Update&quot; button on the left-hand bar.</li>
        </ol>




        <h4> Naming rules for all uploaded files</h4>
        <div> Note that in the templates below, the &quot;_xxx&quot; string stands for the free part
            of the file name. Exact spelling (case-sensitive) needs to be followed</div>
        <ul>
            <li> Data <ul>
                    <li>
                        <span class="tipNote">Real_xxx</span> &#x2014; real, non-human</li>
                    <li>
                        <span class="tipNote">Simulated_xxx</span> &#x2014; simulated</li>
                </ul>
            </li>
            <li> Executable model code <ul>
                    <li>
                        <span class="tipNote">Executable_xxx</span> &#x2014; the model code
                        executable. The executable file is a file which can be executed. With
                        scenario 1, at least two files are executable: the MDL and the PharmML
                        codes. With scenario 2, this name cannot be used for the MDL and PharmML
                        codes. Make sure that the name of the input data in the model code is in
                        agreement with the data file you upload.</li>
                </ul>
            </li>
            <li> Non-executable model code <ul>
                    <li> All files which are not executable must not start with Executable_xxx. In
                        particular: for scenarios 2, and 3, model codes written in MDL or PharmML
                        cannot be named Executable_xxx.</li>
                </ul>
            </li>
            <li> Execution command/script <ul>
                    <li>
                        <span class="tipNote">Command.txt</span> Make sure that the names of the
                        input data, executable code and output file in the script are in agreement
                        with the files you upload.</li>
                </ul>
            </li>
            <li> Output file <ul>
                    <li>
                        <span class="tipNote">Output_real_xxx</span> &#x2014; from execution using
                        realdata. If real data are not uploaded, this is the additional output file
                        introduced above, i.e. a file with the final model parameter estimates and
                        the corresponding uncertainty obtained either from publication or from
                        execution with real data.</li>
                    <li>
                        <span class="tipNote">Output_simulated_xxx</span> &#x2014; from execution
                        usingsimulated data. Make sure that, if shown, the names of the input data
                        and executable code in this file are in agreement with the files you
                        upload.</li>
                </ul>
            </li>
            <li> Modifications with respect to the published model <ul>
                    <li>
                        <span class="tipNote">Model_Accomodations.txt</span> &#x2014; text file
                        describing how the mandatory uploaded model differs from the related
                        publication referred to.</li>
                </ul>
            </li>
        </ul>


        <p> Go to <a href="#scenario1">scenario 1</a> | <a href="#scenario2">scenario 2</a> | <a
                href="#scenario3">scenario 3</a> | <a href="#scenario4">scenario 4</a></p>



        <h3> File content examples</h3>

        <h4> Command.txt &#x2014; example 1</h4>
        <p> Follow this example for the mandatory Command.txt file if scenario 1 aplies. It is
            highly recommended to follow this example with the initial block of commented lines
            filled in.</p>
        <div>
            <pre>
###### Scenario = 1
###### MDL version = &lt;MDL version, constrained to be 7.0 at the moment &gt;
###### IO product = &lt;interoperability product number, constrained to be 4.1 at the moment &gt;
###### Input data = &lt;file name of uploaded input data &gt;
###### Executable model = &lt;file name of uploaded executable &gt;
###### Output = &lt;file name of uploaded output from executable &gt;

#Initialisation
rm(list=ls(all=F)) #clean your workspace first

#Set working directory
myfolder &lt;- &quot;&quot; # name of your project folder
setwd(file.path(Sys.getenv(&quot;MDLIDE_WORKSPACE_HOME&quot;),myfolder))

#Set name of .mdl file and dataset for future tasks
mymodel &lt;- &quot;&quot;
datafile &lt;- &quot;&quot;
mdlfile &lt;- paste0(mymodel,&quot;.mdl&quot;)

#ESTIMATE model parameters using Monolix
mlx &lt;- estimate(mdlfile, target=&quot;MONOLIX&quot;, subfolder=&quot;Monolix&quot;)

# Print the estimated parameters
parameters_mlx &lt;- getPopulationParameters(mlx, what=&quot;estimates&quot;)$MLE
print(mlx)

# Perform model diagnostics for the base model using Xpose functions
mlx.xpdb &lt;- as.xpdb(mlx,datafile)

pdf(&quot;GOF_MLX.pdf&quot;)
print(basic.gof(mlx.xpdb))
print(ind.plots(mlx.xpdb))
dev.off()
# This code can be expanded upon users needs to e.g. include execution in other tools
</pre>
        </div>

        <p> Go to <a href="#scenario1">scenario 1</a> | <a href="#scenario2">scenario 2</a> | <a
                href="#scenario3">scenario 3</a> | <a href="#scenario4">scenario 4</a></p>


        <h4> Command.txt &#x2014; example 2</h4>
        <p> Follow this example for the mandatory Command.txt file if scenario 4 applies. In this
            specific case, the original code is run with NONMEM via PsN.It is highly recommended to
            follow this example with the initial block of commented lines filled in. Additional information can be added as required and/or desired.</p>
        <div>
            <pre>
###### Scenario = 4
###### PsN version = &lt;PsN version&gt;
###### Original tool version = &lt;NONMEM version&gt;
###### Input data = &lt;file name of uploaded input data&gt;
###### Executable model = &lt;file name of uploaded executable&gt;
###### Output = &lt;file name of uploaded output from executable&gt;

execute run1.mod</pre>
        </div>
        <p> Go to <a href="#scenario1">scenario 1</a> | <a href="#scenario2">scenario 2</a> |<a
                href="#scenario3">scenario 3</a> | <a href="#scenario4">scenario 4</a></p>



        <h4> Output_real</h4>
        <p> This is an example for the <span class="tipNote">Output_real_xxx</span> file, to be used
            when the submitter has compiled with the rules of the Repository and has refrained from uploading real human data.
            It can be a text file or a pdf file
            with a table, showing the final model parameter estimates and the corresponding
            uncertainty as provided in the related publication. The text file can be arranged as
            follows,or it can be an excerpt of a table from the publication; however make sure that notation of parameters is consistent with the uploaded model file (if not add explanation in this file):</p>
        <table style="border-collapse: collapse; width:75%; font-style: italic">
            <tbody>
                <tr style="border-collapse: collapse;">
                    <td> Parameter name<br /> [as in the model]</td>
                    <td> Parameter estimate (unit)</td>
                    <td> Relative standard error (%)<br /> [or Standard error; as given in
                        publication]</td>
                </tr>
                <tr style="border-collapse: collapse;">
                    <td> CL</td>
                    <td> 104.5 (L/h)</td>
                    <td> 15%</td>
                </tr>
                <tr style="border-collapse: collapse;">
                    <td> V</td>
                    <td> 25.4 (L)</td>
                    <td> 23%</td>
                </tr>
                <tr style="border-collapse: collapse;">
                    <td> etc.</td>
                    <td> </td>
                    <td> </td>
                </tr>
            </tbody>
        </table>
        <p> Go to <a href="#scenario1">scenario 1</a> | <a href="#scenario2">scenario 2</a> |<a
                href="#scenario3">scenario 3</a> | <a href="#scenario4">scenario 4</a></p>


        <h4> Model_Accommodations.txt &#x2014; example</h4>
        <div>
            <p> This file has to be uploaded when the model is not implemented as in the original
                publication.</p>
            <pre>
###### Deviations from published model:


###### Motivation to deviations:
</pre>
        </div>
        <p> Go to <a href="#scenario1">scenario 1</a> | <a href="#scenario2">scenario 2</a> |<a
                href="#scenario3">scenario 3</a> | <a href="#scenario4">scenario 4</a></p>
        <p> </p>
