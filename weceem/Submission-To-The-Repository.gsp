<h2>
    Submitting to the Repository</h2>
<div class="content pagecontent">
    <p>
        You are about to submit a new model to the DDMoRe Repository.
        The scientific community thanks you!</p>
    <p>
        The following submission guidelines have been defined to guide you through the 
        necessary steps (minimal requirements) you have to
        follow to bring your model into the public domain with high quality information.
        Please read them carefully.  The guidelines are
        <g:link uri="/content/Submission-Guidelines" target="_blank">here</g:link>.</p>
    
    <h3>Terms of agreement</h3>
    
    <ul>
        <li>You are about to submit a new model to the DDMoRe Model Repository.</li>
        <li>Submissions are linked to a [User Name] identified by login information.</li>
        <li>Login information should be accurate and not entered on behalf of someone else.</li>
        <li>It is understood that the submitter is responsible for all submitted material.
            Once the submitter presses the ‘Publish’ button, the submitter agrees to share the
            model, the uploaded data, and any additional file with the public under the terms 
            of the <a target="_blank" href="http://creativecommons.org/publicdomain/zero/1.0/">
                Creative Commons CC0 Public Domain Dedication</a>. Doing so, the submitter is
            waiving all of his or her rights to the work worldwide under copyright law.
            Other users can download, copy, modify, distribute and perform the work, even for
            commercial purposes, all without asking permission and at their own risk.</li>
        
        <li>Unless expressly stated otherwise, the submitter makes no warranties about the 
            work, and disclaims liability for all uses of the work, to the fullest extent
            permitted by applicable law.</li>
        
        <li>If the information provided is not already in public domain, obtaining the 
            approval of co-authors/contributors is the responsibility of the submitter.</li>
        <li>The DDMoRe Foundation accepts no responsibility for the accuracy of the information provided.</li>
        <li>By publishing or sharing a model the submitter agrees on the terms above.</li>
    </ul>
</div>
