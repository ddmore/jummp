<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 30/01/2017
  Time: 19:04
--%>

<g:set var="rvParams" value="${parameters.grep{ it.type == 'rvParam'}}"/>
<g:if test="${!rvParams.isEmpty()}">
    <h4>Random Variables</h4>

    <g:each var="p" in="${rvParams}">
        <div class="spaced-top-bottom">
            <math>
                ${p.maths}
            </math>
        </div>
    </g:each>
</g:if>

<g:set var="simpParams" value="${parameters.grep{ it.type == 'simpleParam'}}"/>
<g:if test="${!simpParams.isEmpty()}">
    <h4>Simple Parameters</h4>

    <g:each var="p" in="${simpParams}">
        <div class="spaced-top-bottom">
            <math>
                ${p.maths}
            </math>
        </div>
    </g:each>
</g:if>

<g:set var="varDefns" value="${parameters.grep{ it.type == 'varDefn'}}"/>
<g:if test="${!varDefns.isEmpty()}">
    <h4>Variables</h4>

    <g:each var="p" in="${varDefns}">
        <div class="spaced-top-bottom">
            <math>
                ${p.maths}
            </math>
        </div>
    </g:each>
</g:if>

<g:set var="popParams" value="${parameters.grep{ it.type == 'popParam'}}"/>
<g:if test="${!popParams.isEmpty()}">
    <h4>Population Parameters</h4>

    <g:each var="p" in="${popParams}">
        <div class="spaced-top-bottom">
            <math>
                ${p.maths}
            </math>
        </div>
    </g:each>
</g:if>

<g:set var="indivParams" value="${parameters.grep{ it.type == 'indivParam'}}"/>
<g:if test="${!indivParams.isEmpty()}">
    <h4>Individual Parameters</h4>

    <g:each var="p" in="${indivParams}">
        <div class="spaced-top-bottom">
            <math>
                ${p.maths}
            </math>
        </div>
    </g:each>
</g:if>

<g:set var="corrs" value="${correlations}"/>
<g:if test="${!corrs.isEmpty()}">
    <h4>Random Variable Correlation</h4>

    <g:each var="p" in="${corrs}">
        <div class="spaced-top-bottom">
            <math>
                ${p.maths}
            </math>
        </div>
    </g:each>
</g:if>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
