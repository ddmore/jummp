<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 30/01/2017
  Time: 19:04
--%>

<div class="spaced-top-bottom">
    <div class="spaced-top-bottom">
        <g:render template="/templates/commonParameters" model="${this}"/>

        <g:set var="obs" value="${observation}"/>
        <h4>Time to Event Observation</h4>
        <table>
            <tr>
                <td>
                    <div class="bold">Event Variable:</div>
                </td>
                <td>
                    <math>
                        <mi>${obs?.symbId}</mi>
                    </math>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="bold">Hazard Function:</div>
                </td>
                <td>
                    <math>
                        ${obs?.maths}
                    </math>
                </td>
            </tr>
        </table>
    </div>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
