<div class="spaced-top-bottom">
    <h2>Step Dependencies</h2>
        <table>
            <tr>
                <th>Step OID</th>
                <th>Preceding Steps</th>
            </tr>
            <g:each var="dep" in="${stepDependencies}">
                <tr>
                    <td>
                        <div><math><mi>${dep.oidRef}</mi></math></div>
                    </td>

                    <td>
                        <g:if test="${dep.depOids}">
                            <div><math>
                        </g:if>
                        <g:each var="ds" in="${dep.depOids}">
                            <mi>${dep.oidRef}</mi>
                        </g:each>
                        <g:if test="${dep.depOids}">
                            </math></div>
                        </g:if>
                    </td>
                </tr>
            </g:each>
        </table>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
