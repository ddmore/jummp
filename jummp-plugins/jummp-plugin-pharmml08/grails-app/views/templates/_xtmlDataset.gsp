<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 14/02/2017
  Time: 14:30
--%>
<div class="spaced-top-bottom">
    <h2>External Dataset</h2>
        <table>
            <tr>
                <td>
                    <div class="bold">OID</div>
                </td>
                <td>
                    <div><math><mi>${oid}</mi></math></div>
                </td>
            </tr>
            <g:if test="${toolname}">
                <tr>
                    <td>
                        <div class="bold">Tool Format</div>
                    </td>
                    <td>
                        <div>${toolname}</div>
                    </td>
                </tr>
            </g:if>
            <g:if test="${description}">
                <tr>
                    <td>
                        <div class="bold">Description</div>
                    </td>
                    <td>
                        <div>${description}</div>
                    </td>
                </tr>
            </g:if>
        </table>

    <g:if test="${dataset.fileSpec}">
        <h3>File Specification</h3>

        <table>
            <tr>
                <td>
                    <div class="bold">Format</div>
                </td>
                <td>
                    <div><math><mi>${dataset.fileSpec.format}</mi></math></div>
                </td>
            </tr>
            <g:if test="${dataset.fileSpec.delimiter}">
                <tr>
                    <td>
                        <div class="bold">Delimiter</div>
                    </td>
                    <td>
                        <div>${dataset.fileSpec.delimiter}</div>
                    </td>
                </tr>
            </g:if>
            <g:if test="${dataset.fileSpec.path}">
                <tr>
                    <td>
                        <div class="bold">File Location</div>
                    </td>
                    <td>
                        <div>${dataset.fileSpec.path}</div>
                    </td>
                </tr>
            </g:if>
        </table>
    </g:if>


    <g:if test="${dataset.definitions}">
        <h3>Column Definitions</h3>

        <table>
            <tr>
                <th>Column ID</th>
                <th>Position</th>
                <th>Column Type</th>
                <th>Value Type</th>
            </tr>
            <g:each var="pte" in="${dataset.definitions}">
                <tr>
                    <td><math><mi>${pte.colId}</mi></math></td>
                    <td>
                        <div><math><mi>${pte.colNum}</mi></math></div>
                    </td>
                    <td>
                        <div><math><mi>${pte.colType}</mi></math></div>
                    </td>
                    <td>
                        <div><math><mi>${pte.valType}</mi></math></div>
                    </td>
                </tr>
            </g:each>
        </table>
    </g:if>

    <g:if test="${columnMappings}">
        <h3>Column Mappings</h3>

        <table>
            <tr>
                <th>Column Ref</th>
                <th>Modelling Mapping</th>
            </tr>
            <g:each var="cm" in="${columnMappings}">
                <tr>
                    <td><div class="spaced-top-bottom"><math display="inline"><mi>${cm.colRef}</mi></math></div></td>
                    <td>
                        <math>
                            <g:if test="${cm.modelMapping}">
                                ${cm.modelMapping}
                            </g:if>
                        </math>
                    </td>
                </tr>
            </g:each>
        </table>
    </g:if>


</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
