<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 30/01/2017
  Time: 19:04
--%>

<div class="spaced-top-bottom">
    <div class="spaced-top-bottom">
        <g:each var="pm" in="${parameterModels}">
            <h3>Parameter Model: <math><mi>${pm.blkId}</mi></math></h3>

            <g:render template="/templates/commonParameters" model="${pm}"/>
        </g:each>
    </div>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
