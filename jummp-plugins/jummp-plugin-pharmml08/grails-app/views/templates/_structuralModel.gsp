<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 14/02/2017
  Time: 14:30
--%>
<div class="spaced-top-bottom">
    <div class="spaced-top-bottom">
        <g:each var="sm" in="${structuralModels}">
            <h3>Structural Model: <math><mi>${sm.blkId}</mi></math></h3>

            <g:set var="simpParams" value="${sm.parameters.grep { it.type == 'simpleParam' }}"/>
            <g:if test="${!simpParams.isEmpty()}">
                <h4>Simple Parameters</h4>

                <g:each var="p" in="${simpParams}">
                    <div class="spaced-top-bottom">
                        <math>
                            ${p.maths}
                        </math>
                    </div>
                </g:each>
            </g:if>

            <g:set var="popParams" value="${sm.parameters.grep { it.type == 'popParam' }}"/>
            <g:if test="${!popParams.isEmpty()}">
                <h4>Population Parameters</h4>

                <g:each var="p" in="${popParams}">
                    <div class="spaced-top-bottom">
                        <math>
                            ${p.maths}
                        </math>
                    </div>
                </g:each>
            </g:if>

            <g:set var="varDefns" value="${sm.variables}"/>
            <g:if test="${!varDefns.isEmpty()}">
                <h4>Variables</h4>

                <g:each var="v" in="${varDefns}">
                    <div class="spaced-top-bottom">
                        <math>
                            ${v.maths}
                        </math>
                    </div>
                </g:each>
            </g:if>

            <g:set var="pkMacroDefns" value="${sm.pkmacros}"/>
            <g:if test="${!pkMacroDefns.isEmpty()}">
                <h4>Variables</h4>

                <g:each var="v" in="${pkMacroDefns}">
                    <div class="spaced-top-bottom">
                        <math>
                            ${v.maths}
                        </math>
                    </div>
                </g:each>
            </g:if>
        </g:each>
    </div>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
