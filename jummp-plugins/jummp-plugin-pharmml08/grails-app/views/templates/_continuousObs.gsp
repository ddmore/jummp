<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 30/01/2017
  Time: 19:04
--%>

<div class="spaced-top-bottom">
    <div class="spaced-top-bottom">
        <g:render template="/templates/commonParameters" model="${this}"/>

        <g:set var="obs" value="${observation}"/>
        <h4>Continuous Observation</h4>

        <div class="spaced-top-bottom">
            <math>
                ${obs.maths}
            </math>
        </div>

    </div>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
