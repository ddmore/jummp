<%@ page import="foundation.ddmore.pharmml2mathml.PharmMlUtils" %>
<div>
 <g:each var="cm" in="${covariateModels}">
    <h3>Covariate Model: <math><mi>${cm.blkId}</mi></math></h3>
    <g:if test="${cm.parameters}">
        <h4>Parameters</h4>
        <!--div class='spaced-top-bottom'-->
            <g:each var="param" in="${cm.parameters}">
                <div class="spaced-top-bottom"><math>${param.maths}</math></div>
            </g:each>
        <!--/div-->
    </g:if>
    <g:if test="${cm.covariates}">
        <!--div class="spaced-top-bottom"-->
            <g:if test="${foundation.ddmore.pharmml2mathml.PharmMlUtils.getContinuousCovariates(cm)}">
                <h4>Continuous Covariates</h4>
                <g:each var="cov" in="${foundation.ddmore.pharmml2mathml.PharmMlUtils.getContinuousCovariates(cm)}">
                    <div class="spaced-top-bottom"><math>${cov.maths}</math></div>
                </g:each>
            </g:if>
            <g:if test="${foundation.ddmore.pharmml2mathml.PharmMlUtils.getCategoricalCovariates(cm)}">
                <h4>Categorical Covariates</h4>
                <g:each var="cov" in="${foundation.ddmore.pharmml2mathml.PharmMlUtils.getCategoricalCovariates(cm)}">
                    <p class='bold default'><span class='italic'>${cov.symbId}</span></p>
                    <div class="spaced-top-bottom">Categories: ${cov.categories}</div>
                </g:each>
            </g:if>
        <!--/div-->
    </g:if>
 </g:each>
    <g:if test="${error}">
        <p>${error}</p>
    </g:if>
</div>
