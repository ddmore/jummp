<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 14/02/2017
  Time: 14:30
--%>
<div class="spaced-top-bottom">
    <h2>Simulation Step</h2>
        <table>
            <tr>
                <td>
                    <div class="bold">OID</div>
                </td>
                <td>
                    <div><math><mi>${oid}</mi></math></div>
                </td>
            </tr>
            <g:if test="${name}">
                <tr>
                    <td>
                        <div class="bold">Name</div>
                    </td>
                    <td>
                        <div>${name}</div>
                    </td>
                </tr>
            </g:if>
            <g:if test="${description}">
                <tr>
                    <td>
                        <div class="bold">Description</div>
                    </td>
                    <td>
                        <div>${description}</div>
                    </td>
                </tr>
            </g:if>
            <g:if test="${extDatasetRef}">
                <tr>
                    <td>
                        <div class="bold">Dataset Reference</div>
                    </td>
                    <td>
                        <div><math><mi>${extDatasetRef}</mi></math></div>
                    </td>
                </tr>
            </g:if>
        </table>

    <h3>Variable Assignments</h3>

    <table>
        <tr>
            <th>Variable</th>
            <th>Value</th>
        </tr>
        <g:each var="pte" in="${variableAssignments}">
            <tr>
                <td>${pte.symbRef}</td>
                <td>
                    <div><math>${pte.maths}</math></div>
                </td>
            </tr>
        </g:each>
    </table>

    <h3>Operations</h3>

    <g:each var="op" in="${operations}">

        <h4>Operation: <math display="inline"><mn>${op.order}</mn></math></h4>

        <table>
            <g:if test="${op.name}">
                <tr>
                    <td>
                        <div class="bold">Name</div>
                    </td>
                    <td>
                        <div>${op.name}</div>
                    </td>
                </tr>
            </g:if>
            <tr>
                <td>
                    <div class="bold">Op Type</div>
                </td>
                <td>
                    <div>${op.opType}</div>
                </td>
            </tr>
        </table>

        <h5>Operation Properties</h5>

        <table>
            <tr>
                <th>Name</th>
                <th>Value</th>
            </tr>
            <g:each var="prop" in="${op.properties}">
                <tr>
                    <td>
                        <div>${prop.name}</div>
                    </td>
                    <td>
                        <div><math>${prop.value}</math></div>
                    </td>
                </tr>
            </g:each>
        </table>

        <g:if test="${op.alg}">
            <h5>Algorithm</h5>

            <table>
                <g:if test="${op.alg?.name}">
                    <tr>
                        <td>
                            <div class="bold">Name</div>
                        </td>
                        <td>
                            <div>${op.alg.name}</div>
                        </td>
                    </tr>
                </g:if>
                <g:if test="${op.alg?.defn}">
                    <tr>
                        <td>
                            <div class="bold">Definition</div>
                        </td>
                        <td>
                            <div>${op.alg.defn}</div>
                        </td>
                    </tr>
                </g:if>
            </table>

            <h6>Algorithm Properties</h6>

            <table>
                <tr>
                    <th>Name</th>
                    <th>Value</th>
                </tr>
                <g:each var="prop" in="${op.alg.properties}">
                    <tr>
                        <td>
                            <div>${prop.name}</div>
                        </td>
                        <td>
                            <div><math>${prop.value}</math></div>
                        </td>
                    </tr>
                </g:each>
            </table>

        </g:if>
    </g:each>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
