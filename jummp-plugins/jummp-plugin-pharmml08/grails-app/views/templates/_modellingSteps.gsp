<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 14/02/2017
  Time: 14:30
--%>

<div id="estimationSteps">
    <g:each var="es" in="${estimationSteps}">
            <g:render template="/templates/estimationStep" model="${es}"/>
    </g:each>

    <g:each var="es" in="${simulationSteps}">
        <g:render template="/templates/simulationStep" model="${es}"/>
    </g:each>

    <g:render template="/templates/stepDependencies" model="${this}"/>

</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
