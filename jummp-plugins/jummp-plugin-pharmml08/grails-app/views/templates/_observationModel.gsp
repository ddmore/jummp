<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 30/01/2017
  Time: 19:04
--%>

<div class="spaced-top-bottom">
    <div class="spaced-top-bottom">
        <g:each var="pm" in="${observationModels}">
            <h3>Observation Model: <math><mi>${pm.blkId}</mi></math></h3>
            <g:if test="${pm.continuousObs}">
                <g:render template="/templates/continuousObs" model="${pm.continuousObs}" />
            </g:if>
            <g:elseif test="${pm.discreteObs}">
                <g:if test="${pm.discreteObs.observation.type == 'count'}">
                    <g:render template="/templates/countObs" model="${pm.discreteObs}" />
                </g:if>
                <g:elseif test="${pm.discreteObs.observation.type == 'categorical'}">
                    <g:render template="/templates/categoricalObs" model="${pm.discreteObs}" />
                </g:elseif>
                <g:elseif test="${pm.discreteObs.observation.type == 'tte'}">
                    <g:render template="/templates/tteObs" model="${pm.discreteObs}" />
                </g:elseif>
            </g:elseif>
        </g:each>
    </div>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
