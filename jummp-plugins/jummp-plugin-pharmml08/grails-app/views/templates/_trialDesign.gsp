<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 14/02/2017
  Time: 14:30
--%>

<div id="trialDesign">
    <g:each var="xtnlDataset" in="${extnlDatasets}">
        <g:render template="/templates/xtmlDataset" model="${xtnlDataset}"/>
    </g:each>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
