<%--
  Created by IntelliJ IDEA.
  User: stumoodie
  Date: 14/02/2017
  Time: 14:30
--%>
<div class="spaced-top-bottom">
    <h2>Estimation Step</h2>
        <table>
            <tr>
                <td>
                    <div class="bold">OID</div>
                </td>
                <td>
                    <div><math><mi>${oid}</mi></math></div>
                </td>
            </tr>
            <g:if test="${name}">
                <tr>
                    <td>
                        <div class="bold">Name</div>
                    </td>
                    <td>
                        <div>${name}</div>
                    </td>
                </tr>
            </g:if>
            <g:if test="${description}">
                <tr>
                    <td>
                        <div class="bold">Description</div>
                    </td>
                    <td>
                        <div>${description}</div>
                    </td>
                </tr>
            </g:if>
            <tr>
                <td>
                    <div class="bold">Dataset Reference</div>
                </td>
                <td>
                    <div><math><mi>${extDatasetRef}</mi></math></div>
                </td>
            </tr>
        </table>

    <h3>Parameters To Estimate</h3>

    <table>
        <tr>
            <th>Parameter</th>
            <th>Initial Value</th>
            <th>Fixed?</th>
            <th>Limits</th>
        </tr>
        <g:each var="pte" in="${parametersToEstimate}">
            <tr>
                <td>${pte.symbRef}</td>
                <td>
                    <g:if test="${pte.initialValue.value}">
                        <div><math>${pte.initialValue.value}</math></div>
                    </g:if>
                </td>
                <td>
                    <div>${pte.initialValue.fixed}</div>
                </td>
                <td>
                    <div><math><mfenced><mrow><g:if test="${pte.lower}">${pte.lower}</g:if></mrow><mrow><g:if test="${pte.upper}">${pte.upper}</g:if></mrow></mfenced></math></div>
                </td>
            </tr>
        </g:each>
    </table>

    <h3>Operations</h3>

    <g:each var="op" in="${operations}">

        <h4>Operation: <math display="inline"><mn>${op.order}</mn></math></h4>

        <table>
            <!--tr>
                <td>
                    <div class="bold">Order</div>
                </td>
                <td>
                    <div><math><mn>${op.order}</mn></math></div>
                </td>
            </tr-->
            <g:if test="${op.name}">
                <tr>
                    <td>
                        <div class="bold">Name</div>
                    </td>
                    <td>
                        <div>${op.name}</div>
                    </td>
                </tr>
            </g:if>
            <tr>
                <td>
                    <div class="bold">Op Type</div>
                </td>
                <td>
                    <div>${op.opType}</div>
                </td>
            </tr>
        </table>

        <h5>Operation Properties</h5>

        <table>
            <tr>
                <th>Name</th>
                <th>Value</th>
            </tr>
            <g:each var="prop" in="${op.properties}">
                <tr>
                    <td>
                        <div>${prop.name}</div>
                    </td>
                    <td>
                        <div><math>${prop.value}</math></div>
                    </td>
                </tr>
            </g:each>
        </table>

        <g:if test="${op.alg}">
            <h5>Algorithm</h5>

            <table>
                <g:if test="${op.alg?.name}">
                    <tr>
                        <td>
                            <div class="bold">Name</div>
                        </td>
                        <td>
                            <div>${op.alg.name}</div>
                        </td>
                    </tr>
                </g:if>
                <g:if test="${op.alg?.defn}">
                    <tr>
                        <td>
                            <div class="bold">Definition</div>
                        </td>
                        <td>
                            <div>${op.alg.defn}</div>
                        </td>
                    </tr>
                </g:if>
            </table>

            <h6>Algorithm Properties</h6>

            <table>
                <tr>
                    <th>Name</th>
                    <th>Value</th>
                </tr>
                <g:each var="prop" in="${op.alg.properties}">
                    <tr>
                        <td>
                            <div>${prop.name}</div>
                        </td>
                        <td>
                            <div><math>${prop.value}</math></div>
                        </td>
                    </tr>
                </g:each>
            </table>

        </g:if>
    </g:each>
</div>
<g:if test="${error}">
    <p>${error}</p>
</g:if>
