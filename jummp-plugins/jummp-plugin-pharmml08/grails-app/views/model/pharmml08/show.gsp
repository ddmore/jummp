<%--
 Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),
 Deutsches Krebsforschungszentrum (DKFZ)

 This file is part of Jummp.

 Jummp is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free
 Software Foundation; either version 3 of the License, or (at your option) any
 later version.

 Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along
 with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
--%>

<head>
    <meta name="layout" content="modelDisplay"/>
    <style>
        td, th {
            padding: 5px;
        }
        th {
            text-align: center;
            vertical-align: middle;
        }
        .default {
            margin:0;
        }
        .bold {
            font-weight: bold;
        }
        .italic {
            font-style: italic;
        }
    </style>
</head>
<content tag="genericAnnotations">
    <anno:renderGenericAnnotations annotations="${genericAnnotations}"/>
</content>
<content tag="modelspecifictabs">
    <pharmml08:decideTabs md="${pharmmlDoc.modelDefinition}"
            td="${pharmmlDoc.trialDesign}" est="${pharmmlDoc.modellingSteps.estimationSteps}" sim="${pharmmlDoc.modellingSteps.simulationSteps}" />
</content>
<content tag="modelspecifictabscontent">
    <pharmml08:handleModelDefinitionTab version="${version}" pharmmlDoc="${pharmmlDoc}" td="${pharmmlDoc.trialDesign}" est="${pharmmlDoc.modellingSteps.estimationSteps}" sim="${pharmmlDoc.modellingSteps.simulationSteps}"/>

    <g:if test="${pharmmlDoc.trialDesign}">
        <g:render template="/templates/trialDesign" model="${pharmmlDoc.trialDesign}"/>
    </g:if>

    <g:if test="${pharmmlDoc.modellingSteps}">
        <g:render template="/templates/modellingSteps" model="${pharmmlDoc.modellingSteps}"/>
    </g:if>

    <!--pharmml08:handleTrialDesignTab version="${version}" ts="${structure}"
            td="${dosing}" tp="${population}" rev="${revision}"/-->

    <!--pharmml08:handleModellingStepsTabs estimation="${estSteps}" simulation="${simSteps}"
        independentVariable="${independentVar}" deps="${stepDeps}" version="${version}"
        rev="${revision}"/-->
</content>
