/**
 * Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 *
 * Additional permission under GNU Affero GPL version 3 section 7
 *
 * If you modify Jummp, or any covered work, by linking or combining it with
 * LibPharmml (or a modified version of that library), containing parts
 * covered by the terms of Apache License v2.0, the licensors of this
 * Program grant you additional permission to convey the resulting work.
 * {Corresponding Source for a non-source form of such a combination shall
 * include the source code for the parts of LibPharmml used as well as
 * that of the covered work.}
 **/





package net.biomodels.jummp.plugins.pharmml08

class PharmMl08TagLib {
    static namespace = "pharmml08"

    def decideTabs = { attrs ->
        boolean haveTabsToDisplay = true
        def topics = ["md", "td", "est", "sim"]
        topics.inject(haveTabsToDisplay) { display, t ->
            display && attrs."${t}"
        }
        if (!haveTabsToDisplay) {
            return
        }
        //holds information about which PharmML-specific tabs should be shown
        Map<String, String> tabsMap = new HashMap<>(topics.size(), 1.0)
        if (attrs.md) {
            final String MDEF_TAB = "modelDefinition"
            tabsMap["mdef"] = MDEF_TAB
            out << "<li><a href='#${MDEF_TAB}'>Model Definition</a></li>\n"
        }
        if (attrs.td) {
            final String TD_TAB = "trialDesign"
            tabsMap["td"] = TD_TAB
            out << "<li><a href='#${TD_TAB}'>Trial Design</a></li>\n"
        }
        if (attrs.est || attrs.sim) {
            final String EST_TAB = "estimationSteps"
            tabsMap["est"] = EST_TAB
            out << "<li><a href='#${EST_TAB}'>Modelling Steps</a></li>\n"
        }
//        if (attrs.sim) {
//            final String SIM_TAB = "simulationSteps"
//            tabsMap["sim"] = SIM_TAB
//            out << "<li><a href='#${SIM_TAB}'>Simulation Steps</a></li>\n"
//        }
        pageScope.tabsMap = tabsMap
    }

    def handleModelDefinitionTab = { attrs ->
        if (!pageScope.tabsMap["mdef"]) {
            return
        }
        out << "<div id='${pageScope.tabsMap["mdef"]}'>"
        out << render(template: "/templates/header",  model: attrs.pharmmlDoc)
        out << render(template: "/templates/functionDefinitions",  model: [funcDefns: attrs.pharmmlDoc.funcDefns])
        out << render(template: "/templates/covariateModel", model: attrs.pharmmlDoc.modelDefinition)
        out << render(template: "/templates/parameterModel", model: attrs.pharmmlDoc.modelDefinition)
        out << render(template: "/templates/structuralModel", model: attrs.pharmmlDoc.modelDefinition)
        out << render(template: "/templates/observationModel", model: attrs.pharmmlDoc.modelDefinition)
        out << "</div>\n"
    }


}

