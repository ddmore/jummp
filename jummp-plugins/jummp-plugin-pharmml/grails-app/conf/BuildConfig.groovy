/**
* Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),
* Deutsches Krebsforschungszentrum (DKFZ)
*
* This file is part of Jummp.
*
* Jummp is free software; you can redistribute it and/or modify it under the
* terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
* A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License along
* with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
**/





grails.servlet.version = "2.5"
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.source.level = 1.7
grails.project.target.level = 1.7
grails.project.dependency.resolver = "maven"
grails.compile.artefacts.closures.convert = false

grails.project.fork = [
    // configure settings for the test-app JVM, uses the daemon by default
    test: false, //[maxMemory: 2048, minMemory: 64, debug: false, maxPerm: 512, daemon:true],
    // configure settings for the run-app JVM
    run: [maxMemory: 2048, minMemory: 64, debug: false, maxPerm: 512, forkReserve:false],
    // configure settings for the run-war JVM
    war: [maxMemory: 2048, minMemory: 64, debug: false, maxPerm: 512, forkReserve:false],
    // configure settings for the Console UI JVM
    console: [maxMemory: 1024, minMemory: 64, debug: false, maxPerm: 256]
]

grails.project.dependency.resolution = {
    def gebVersion = "1.0"
    def seleniumVersion = "2.52.0"

    inherits("global") {
    }
    log "error"
    legacyResolve false
    checksums true
    repositories {
        inherits true
//        if (System.getenv("JUMMP_ARTIFACTORY_URL")) {
//            mavenRepo "${System.getenv('JUMMP_ARTIFACTORY_URL')}"
//        }
        grailsPlugins()
        grailsHome()
        mavenLocal()
        grailsCentral()
        mavenCentral()
//        mavenRepo "http://www.ebi.ac.uk/~maven/m2repo"
//        mavenRepo "http://www.ebi.ac.uk/~maven/m2repo_snapshots/"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }
    dependencies {
//        compile "net.biomodels.jummp:AnnotationStore:0.2.9-SNAPSHOT"
//        compile ("eu.ddmore.metadata:lib-metadata:1.5.2-SNAPSHOT") {
//            excludes 'spring-context','spring-core','spring-test', 'slf4j-log4j12'
//        }
//        compile("eu.ddmore.pharmml:libPharmML:0.7.3-1")
        compile("eu.ddmore.pharmml:libPharmML:0.4.5-b1")
        runtime("commons-jexl:commons-jexl:1.1") { excludes 'junit', 'commons-logging' }
        compile "org.apache.tika:tika-core:1.3"
        compile 'org.codehaus.groovy:groovy-backports-compat23:2.3.11'
        test "org.grails:grails-datastore-test-support:1.0-grails-2.3"
        test "org.gebish:geb-spock:$gebVersion"
        test "org.seleniumhq.selenium:selenium-support:$seleniumVersion"
        test ("org.seleniumhq.selenium:selenium-chrome-driver:$seleniumVersion"){
            exclude 'xml-apis'
        }
        test "io.github.bonigarcia:webdrivermanager:1.5.0"
        def httpComponentsVersion = '4.3.2'
        compile "org.apache.httpcomponents:httpclient:$httpComponentsVersion"
        compile "org.apache.httpcomponents:httpcore:$httpComponentsVersion"
        compile "org.apache.httpcomponents:httpmime:$httpComponentsVersion"
    }

    plugins {
        compile ":perf4j:0.1.1"
        build ":tomcat:7.0.54"
        test ":geb:$gebVersion"
    }
}
grails.plugin.location.'jummp-plugin-security' = "../jummp-plugin-security"
grails.plugin.location.'jummp-plugin-core-api' = "../jummp-plugin-core-api"
grails.plugin.location.'jummp-plugin-configuration' = "../jummp-plugin-configuration"
