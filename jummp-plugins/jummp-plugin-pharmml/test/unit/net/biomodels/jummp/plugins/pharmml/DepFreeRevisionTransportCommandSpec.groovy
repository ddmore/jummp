package net.biomodels.jummp.plugins.pharmml

import net.biomodels.jummp.core.model.ModelFormatTransportCommand
import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.core.model.ModelTransportCommand
import net.biomodels.jummp.core.model.RepositoryFileTransportCommand
import net.biomodels.jummp.core.model.ValidationState
import org.junit.Test

/**
 * Created by smoodie on 08/12/2016.
 */
class DepFreeRevisionTransportCommandSpec extends GroovyTestCase {

    @Test
    void test_files_getter(){
        given:  " A newly created command object"
        def modelName = "A name"
        def formatCommand = new ModelFormatTransportCommand(
            id: 16L,
            identifier: "PharmML",
            name: "PharmML",
            formatVersion: "0.6"
        )
        def modelCommand = new ModelTransportCommand(
            id: 1L,
            submissionId: "MODEL00000001",
            publicationId: null,
            name: modelName,
            deleted: false,
            format: formatCommand,
            comment: null,
            lastModifiedDate: new Date(),
            publication: null,
            submitter: "Stuart Moodie",
            submissionDate: new Date(),
            firstPublished: new Date(),
            creators: [ "Stuart Moodie" ] as Set,
            creatorUsernames: [ "smoodie" ] as Set,
            state: ModelState.PUBLISHED,
            submitterUsername: "smoodie",
            flagLevel: null
        )
        def valState = ValidationState.APPROVED
        def RepositoryFileTransportCommand file1 = new RepositoryFileTransportCommand(
            id: 1L,
            path: "/User/smoodie/test.xml",
            description: "Desn",
            hidden: false,
            mainFile: true,
            userSubmitted: true,
            mimeType: 'plain/txt'
        )
        def cmd =  new DepFreeRevisionTransportCommand(
            id: 1,
            state: ModelState.PUBLISHED,
            revisionNumber: 1,
            owner: "Stuart Moodie",
            minorRevision: false,
            validated: true,
            name: modelName,
            description: "Descn",
            comment: "A Comment",
            uploadDate: new Date(),
            format: formatCommand,
            model: modelCommand,
            files: [ file1 ] as List,
            annotations: [],
            validationLevel: valState,
            validationReport: null,
            qcInfo: null
        )
        when: "Nothing needs doing"

        then: "expect files to be as set"
        cmd.files != null
        cmd.files.size() > 0
        cmd.files.head() == file1
    }

}
