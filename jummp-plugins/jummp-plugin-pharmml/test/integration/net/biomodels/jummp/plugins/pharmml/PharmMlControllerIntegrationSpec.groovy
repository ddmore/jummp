package net.biomodels.jummp.plugins.pharmml

import net.biomodels.jummp.core.model.ModelFormatTransportCommand
import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.core.model.ModelTransportCommand
import net.biomodels.jummp.core.model.RepositoryFileTransportCommand
import net.biomodels.jummp.core.model.RevisionTransportCommand
import net.biomodels.jummp.core.model.ValidationState


/**
 * Created by smoodie on 07/12/2016.
 */
class PharmMlControllerIntegrationSpec extends GroovyTestCase {

    void testSomething(){
        def modelName = "A name"
        def formatCommand = new ModelFormatTransportCommand(
            id: 16L,
            identifier: "PharmML",
            name: "PharmML",
            formatVersion: "0.6"
        )
        def modelCommand = new ModelTransportCommand(
            id: 1L,
            submissionId: "MODEL00000001",
            publicationId: null,
            name: modelName,
            deleted: false,
            format: formatCommand,
            comment: null,
            lastModifiedDate: new Date(),
            publication: null,
            submitter: "Stuart Moodie",
            submissionDate: new Date(),
            firstPublished: new Date(),
            creators: [ "Stuart Moodie" ] as Set,
            creatorUsernames: [ "smoodie" ] as Set,
            state: ModelState.PUBLISHED,
            submitterUsername: "smoodie",
            flagLevel: null
        )
        def valState = ValidationState.APPROVED
        def RepositoryFileTransportCommand file1 = new RepositoryFileTransportCommand(
            id: 1L,
            path: "test/files/0.6/timeToEvent_weibullHazard.xml",
            description: "Desn",
            hidden: false,
            mainFile: true,
            userSubmitted: true,
            mimeType: 'plain/txt'
        )
        def rev =  new DepFreeRevisionTransportCommand(
            id: 1,
            state: ModelState.PUBLISHED,
            revisionNumber: 1,
            owner: "Stuart Moodie",
            minorRevision: false,
            validated: true,
            name: modelName,
            description: "Descn",
            comment: "A Comment",
            uploadDate: new Date(),
            format: formatCommand,
            model: modelCommand,
            files: [ file1 ],
            annotations: [],
            validationLevel: valState,
            validationReport: null,
            qcInfo: null
        )
        def model = [revision: rev,
                     authors: "",
                     allRevs: [ "0.6" ],
                     flashMessage: "",
                     canUpdate: false,
                     canDelete: false,
                     canShare: false,
                     showPublishOption: true,
                     canSubmitForPublication: false,
                     canCertify: true,
                     validationLevel: valState,
                     certComment: "Cert"
        ]
        def controller = new PharmMlController()
        controller.flash.genericModel = model
        controller.show()
        def mv = controller.getModelAndView()
        assertEquals("expected URL", "/model/pharmml/show", mv.viewName)
    }

}

class DepFreeRevisionTransportCommand extends RevisionTransportCommand{

    @Override
    List<RepositoryFileTransportCommand> getFiles(){
        super.@files
    }

}
