drop database if exists jummpdb;
create database jummpdb;
GRANT ALL PRIVILEGES ON jummpdb.* TO 'jummpdbadmin'@'localhost';
FLUSH PRIVILEGES;

use jummpdb;
source jummp_dump.sql
