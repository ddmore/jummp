-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: jummpdb
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DATABASECHANGELOG`
--

DROP TABLE IF EXISTS `DATABASECHANGELOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATABASECHANGELOG` (
  `ID` varchar(63) NOT NULL,
  `AUTHOR` varchar(63) NOT NULL,
  `FILENAME` varchar(200) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`AUTHOR`,`FILENAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOG`
--

LOCK TABLES `DATABASECHANGELOG` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOG` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOG` VALUES ('1392901757860-1','mglont (generated)','changelog.groovy','2016-09-07 11:57:14',1,'EXECUTED','3:1d3e1f001e55c50abf9e18b6d72ab8b3','Create Table','',NULL,'2.0.5'),('1392901757860-10','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',10,'EXECUTED','3:e6d46cc9d7ab6314afe7cea130b00882','Create Table','',NULL,'2.0.5'),('1392901757860-11','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',11,'EXECUTED','3:66a364c95215821a6eb6dcff435a5e05','Create Table','',NULL,'2.0.5'),('1392901757860-12','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',12,'EXECUTED','3:c5dea796184d29ac25e5f4caa3a2e929','Create Table','',NULL,'2.0.5'),('1392901757860-13','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',13,'EXECUTED','3:a3bcd9b02658ae7a3b9dfbbe251a972d','Create Table','',NULL,'2.0.5'),('1392901757860-14','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',14,'EXECUTED','3:dd1e083f42a1d5bebc474a5c186df488','Create Table','',NULL,'2.0.5'),('1392901757860-15','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',15,'EXECUTED','3:ae78f64457b989b2d1afc123bb62bcce','Create Table','',NULL,'2.0.5'),('1392901757860-16','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',16,'EXECUTED','3:cceb1d3359d16b3575a454782c794b4c','Create Table','',NULL,'2.0.5'),('1392901757860-17','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',17,'EXECUTED','3:cad3e283b4fba0808fdeeb2727e28f44','Create Table','',NULL,'2.0.5'),('1392901757860-18','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',18,'EXECUTED','3:3de98a5b3d169334e47fc5081f745889','Create Table','',NULL,'2.0.5'),('1392901757860-19','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',19,'EXECUTED','3:8db439f50d91e070ce41e34a959c4165','Create Table','',NULL,'2.0.5'),('1392901757860-2','mglont (generated)','changelog.groovy','2016-09-07 11:57:14',2,'EXECUTED','3:307aba3673312e6f4bd3d729d433fcc6','Create Table','',NULL,'2.0.5'),('1392901757860-20','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',20,'EXECUTED','3:51977d6d3fc4f6de43807e1358108731','Create Table','',NULL,'2.0.5'),('1392901757860-21','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',21,'EXECUTED','3:b277f5e3d86419d173092934da2e2026','Create Table','',NULL,'2.0.5'),('1392901757860-22','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',22,'EXECUTED','3:8593e8d7a0d29e5c8480c875cd82dd41','Create Table','',NULL,'2.0.5'),('1392901757860-23','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',23,'EXECUTED','3:f92709b13553041770ce4bd1c6b1490d','Create Table','',NULL,'2.0.5'),('1392901757860-24','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',24,'EXECUTED','3:a18d6dcde166c410f2e36dd48963016f','Create Table','',NULL,'2.0.5'),('1392901757860-25','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',25,'EXECUTED','3:2abba9672017043f0287c359af31d3b5','Create Table','',NULL,'2.0.5'),('1392901757860-26','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',26,'EXECUTED','3:17dc0451866c84725372b5b9d31fa944','Create Table','',NULL,'2.0.5'),('1392901757860-27','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',27,'EXECUTED','3:1ac65eceafb891cd1494831e1af1a7a1','Create Table','',NULL,'2.0.5'),('1392901757860-28','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',28,'EXECUTED','3:a4e6a987db1ff108059da9738b085e91','Create Table','',NULL,'2.0.5'),('1392901757860-29','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',29,'EXECUTED','3:6b24aac651c14a66e89f26a21e6c3e40','Create Table','',NULL,'2.0.5'),('1392901757860-3','mglont (generated)','changelog.groovy','2016-09-07 11:57:14',3,'EXECUTED','3:5ec5ba59aeaf379758413d1ef6b54c36','Create Table','',NULL,'2.0.5'),('1392901757860-30','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',30,'EXECUTED','3:133cfd609ef82ba7165d8b2b1373ba53','Create Table','',NULL,'2.0.5'),('1392901757860-31','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',31,'EXECUTED','3:edfd4ed7ba4377ea37fb314db3a7c8a8','Add Primary Key','',NULL,'2.0.5'),('1392901757860-32','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',32,'EXECUTED','3:9f5cf96e5aa930b8e2ba785bb19e6302','Add Primary Key','',NULL,'2.0.5'),('1392901757860-33','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',57,'EXECUTED','3:fea775d81547b2be31c12f6345d8942d','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-34','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',58,'EXECUTED','3:cb77c3d1073d26fda2aef6038f5a1a54','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-35','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',59,'EXECUTED','3:4fd9d53b183695220e726a5a7eaeeef0','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-36','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',60,'EXECUTED','3:40c7714ebd9a5a11b5786cb514fb1a04','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-37','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',61,'EXECUTED','3:2b55ff2803f7853d294b8bf0e4587219','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-38','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',62,'EXECUTED','3:3d89134d9254d95eb3c68f7c60a63590','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-39','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',63,'EXECUTED','3:00f4a9234f5c38fe064781b45e2c03dd','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-4','mglont (generated)','changelog.groovy','2016-09-07 11:57:14',4,'EXECUTED','3:3a892288cc7b6a1c85756b37e2e8bae9','Create Table','',NULL,'2.0.5'),('1392901757860-40','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',64,'EXECUTED','3:3ef151e3671b3d6e3b7bf5f29c74cf93','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-41','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',65,'EXECUTED','3:de53968737bb17cf28b7c1d5b83f9954','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-42','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',66,'EXECUTED','3:37f3bb2c364792e1ff9a9f3bb5811b41','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-43','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',67,'EXECUTED','3:d640b670818c6490a117ce693bd8682b','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-44','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',68,'EXECUTED','3:cab7be1debab83faa3c3ea72de4c2c4f','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-45','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',69,'EXECUTED','3:9e2e3f5f50ba823d56fdb15f1a17bf54','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-46','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',70,'EXECUTED','3:4c6796243a2a5b2bd22436c23118da32','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-47','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',71,'EXECUTED','3:b4bfb35fcd34239523b051171c220360','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-48','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',72,'EXECUTED','3:cf175709dcecfb34171d15770653687d','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-49','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',73,'EXECUTED','3:473b23ae22405bc2ab92fa0099cf6a39','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-5','mglont (generated)','changelog.groovy','2016-09-07 11:57:14',5,'EXECUTED','3:dcfab9316b023b395a88f734ab495a57','Create Table','',NULL,'2.0.5'),('1392901757860-50','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',74,'EXECUTED','3:cd9f0f89c387191702d8060833bfd4ce','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-51','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',75,'EXECUTED','3:0d2d58b39c17260ea5d33de113839339','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-52','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',76,'EXECUTED','3:ab2234f6ac879b8364f538508503606b','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-53','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',77,'EXECUTED','3:14beba9ad8b1d170bec5469fbc47cc70','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-54','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',78,'EXECUTED','3:b5cd2cb17a24b9453ec944d59b01a2d1','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-55','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',79,'EXECUTED','3:e89d6a1f5a9ba00fd1b13db244fd0769','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-56','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',80,'EXECUTED','3:99fe3da9d8aa8e87b432fb0a3233ca8e','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-57','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',81,'EXECUTED','3:30fd5de4f9339bfb36576f7be2192c2e','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-58','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',82,'EXECUTED','3:22cc727dc26fbe6ec5805acd0b390f3c','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-59','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',83,'EXECUTED','3:a688464fa6e1e07d13e4537a0774b136','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-6','mglont (generated)','changelog.groovy','2016-09-07 11:57:14',6,'EXECUTED','3:d5c98cffcbf398a275ce0d44eb0a14bb','Create Table','',NULL,'2.0.5'),('1392901757860-60','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',84,'EXECUTED','3:3545d9cd2cbcac12b5a7e23ade8bef10','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-61','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',85,'EXECUTED','3:9277004f771c06673354838ad848e0b0','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-62','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',86,'EXECUTED','3:03367dafc1cf835ab940ac725427cfcf','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-63','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',87,'EXECUTED','3:d7d6eb2112d37e9d630210d10113eb5d','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-64','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',88,'EXECUTED','3:4656fa3c241b1048e59b06babd7e6c88','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-65','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',89,'EXECUTED','3:1a7c235e3c52325159c4ff267d9794ce','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-66','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',90,'EXECUTED','3:ff69a493e9aafab0b5308771a8f10921','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-67','mglont (generated)','changelog.groovy','2016-09-07 11:57:17',91,'EXECUTED','3:8e267915561bd6c2da6540600f2cd338','Add Foreign Key Constraint','',NULL,'2.0.5'),('1392901757860-68','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',33,'EXECUTED','3:c44fbe0e83dd1027f2b027f6df81c72c','Create Index','',NULL,'2.0.5'),('1392901757860-69','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',34,'EXECUTED','3:d96940a4872f3fea1469471dec79e6a7','Create Index','',NULL,'2.0.5'),('1392901757860-7','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',7,'EXECUTED','3:20dfe0eb2b6de469cf54c5917d3f7dd8','Create Table','',NULL,'2.0.5'),('1392901757860-70','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',35,'EXECUTED','3:081fb2f011d93a6017a35be1e943cea7','Create Index','',NULL,'2.0.5'),('1392901757860-71','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',36,'EXECUTED','3:2cc5e06002bc371de101171a61e3f3eb','Create Index','',NULL,'2.0.5'),('1392901757860-72','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',37,'EXECUTED','3:3e0414e745b2b684e1e522140437200a','Create Index','',NULL,'2.0.5'),('1392901757860-73','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',38,'EXECUTED','3:cad2b9e84b48833ea0505a45206cb1f8','Create Index','',NULL,'2.0.5'),('1392901757860-74','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',39,'EXECUTED','3:6c209219963374b8a7d87ca1d8048e5b','Create Index','',NULL,'2.0.5'),('1392901757860-75','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',40,'EXECUTED','3:41d16a0fde354e760afe1b1f127e174c','Create Index','',NULL,'2.0.5'),('1392901757860-76','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',41,'EXECUTED','3:c6d292365fd1bb16cc6dbcb8e1800271','Create Index','',NULL,'2.0.5'),('1392901757860-77','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',42,'EXECUTED','3:acbec5695a43af93c986cb2e02675bbc','Create Index','',NULL,'2.0.5'),('1392901757860-78','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',43,'EXECUTED','3:2cfd1c42a0ded8afff5431ccd5427ba5','Create Index','',NULL,'2.0.5'),('1392901757860-79','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',44,'EXECUTED','3:9ac8d289994dfc2e0dc5f8bc6d2b629d','Create Index','',NULL,'2.0.5'),('1392901757860-8','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',8,'EXECUTED','3:ecbc433486c8ff088a4be247f1849835','Create Table','',NULL,'2.0.5'),('1392901757860-80','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',45,'EXECUTED','3:0c9eee06e6566d03b1af57bd9c3de367','Create Index','',NULL,'2.0.5'),('1392901757860-81','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',46,'EXECUTED','3:4596099fe351f3da0a8ef43d7ed7b548','Create Index','',NULL,'2.0.5'),('1392901757860-82','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',47,'EXECUTED','3:32ce829d2f9503bd2f396b408d583fe6','Create Index','',NULL,'2.0.5'),('1392901757860-83','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',48,'EXECUTED','3:f2887536d3733ca545559a741fd46cd1','Create Index','',NULL,'2.0.5'),('1392901757860-84','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',49,'EXECUTED','3:b5aff080da3bb6a7fb6887a93ce3bdff','Create Index','',NULL,'2.0.5'),('1392901757860-85','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',50,'EXECUTED','3:fef9bd5300bcfefcd0fdfa6f8fc90a98','Create Index','',NULL,'2.0.5'),('1392901757860-86','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',51,'EXECUTED','3:e0ffbca58c0af3668d1b2378eb90ff54','Create Index','',NULL,'2.0.5'),('1392901757860-87','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',52,'EXECUTED','3:4746f33c36843f9a8d35a62c27af3ab2','Create Index','',NULL,'2.0.5'),('1392901757860-88','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',53,'EXECUTED','3:a51a71e7288c953251ac52e9dfc12bc7','Create Index','',NULL,'2.0.5'),('1392901757860-89','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',54,'EXECUTED','3:83db31c447aa267b4274a8cfb292c03c','Create Index','',NULL,'2.0.5'),('1392901757860-9','mglont (generated)','changelog.groovy','2016-09-07 11:57:15',9,'EXECUTED','3:e1fd14734052028d3de2a38475bf47f0','Create Table','',NULL,'2.0.5'),('1392901757860-90','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',55,'EXECUTED','3:f44df2b44c51d1376f0e0c03d453600e','Create Index','',NULL,'2.0.5'),('1392901757860-91','mglont (generated)','changelog.groovy','2016-09-07 11:57:16',56,'EXECUTED','3:7b336843c2af2ad6bb3a1c2ff9a08db0','Create Index','',NULL,'2.0.5'),('1392928519622-2','mglont (generated)','author2person.groovy','2016-09-07 11:57:17',94,'EXECUTED','3:a0224aa352a9dc74c8791c9ff7e77c39','Create Table','',NULL,'2.0.5'),('1392928519622-3','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',96,'EXECUTED','3:a9cb2bcd382b4e541a76859bb87c57fd','Add Column','',NULL,'2.0.5'),('1392928519622-4','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',98,'EXECUTED','3:0ff098ae778fb0dec3959a466de22929','Add Not-Null Constraint','',NULL,'2.0.5'),('1393423756842-1','mglont (generated)','author2person.groovy','2016-09-07 11:57:17',92,'EXECUTED','3:b8f14018b7b9a7d00b326c25b8edadb2','Create Table','',NULL,'2.0.5'),('1393423756842-11','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',101,'EXECUTED','3:9f72d1c9f814e9e87f1ccf2a9be6f7b3','Create Index','',NULL,'2.0.5'),('1393423756842-13','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',102,'EXECUTED','3:3b239c019553396df4b2442f06c109e5','Drop Column','',NULL,'2.0.5'),('1393423756842-14','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',103,'EXECUTED','3:efc01eff6cc113dd535deb8326e1a511','Drop Column','',NULL,'2.0.5'),('1393423756842-15','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',104,'EXECUTED','3:4b5a3c1e8e2ae5cf6f2001b6ad378685','Drop Column','',NULL,'2.0.5'),('1393423756842-16','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',105,'EXECUTED','3:b222dbc20c8cc64f232a821983415b47','Drop Table','',NULL,'2.0.5'),('1393423756842-17','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',106,'EXECUTED','3:53590bfdeb528ef8715acd82fc454ead','Drop Table','',NULL,'2.0.5'),('1393423756842-5','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',99,'EXECUTED','3:c88c7ba508c4615111b00c88e6185dae','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1393423756842-6','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',100,'EXECUTED','3:6b96c5a00574be32fcd68695c343ea7a','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1393423756842-7','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',107,'EXECUTED','3:4c9599005383b5447f9f771164a0af49','Add Foreign Key Constraint','',NULL,'2.0.5'),('1393423756842-8','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',108,'EXECUTED','3:5969e90f5cf86b526226777dd55dd35e','Add Foreign Key Constraint','',NULL,'2.0.5'),('1393423756842-9','mglont (generated)','author2person.groovy','2016-09-07 11:57:18',109,'EXECUTED','3:05424f3f7792c15e1cc158cde451b84c','Add Foreign Key Constraint','',NULL,'2.0.5'),('1393841722118-1','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',110,'EXECUTED','3:f341a72a4a77c4fabf721a160f2371cb','Drop Index','',NULL,'2.0.5'),('1393841722118-10','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',119,'EXECUTED','3:5e44288152cfeea7971956ea5ff929e9','Create Index','',NULL,'2.0.5'),('1393841722118-11','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',120,'EXECUTED','3:f0127639d3ecf3622660639b746354d2','Create Index','',NULL,'2.0.5'),('1393841722118-2','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',111,'EXECUTED','3:2289b2164cf207721b9dc827fc59909c','Create Index','',NULL,'2.0.5'),('1393841722118-3','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',112,'EXECUTED','3:5b27a46331a752bc242c23107c80db60','Create Index','',NULL,'2.0.5'),('1393841722118-4','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',113,'EXECUTED','3:4c5b8592457a719a3c4962aef40e3db7','Create Index','',NULL,'2.0.5'),('1393841722118-5','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',114,'EXECUTED','3:56c10054b4304dd130f4f52c3e42686f','Create Index','',NULL,'2.0.5'),('1393841722118-6','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',115,'EXECUTED','3:6672c46ecd5d66a4ee487a308e14fafe','Create Index','',NULL,'2.0.5'),('1393841722118-7','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',116,'EXECUTED','3:bccfffb06bd9e1d6aba89b440f8afa79','Create Index','',NULL,'2.0.5'),('1393841722118-8','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',117,'EXECUTED','3:c8fac308bd561dcb8bad9c6cbd210691','Create Index','',NULL,'2.0.5'),('1393841722118-9','mglont (generated)','missing_indices.groovy','2016-09-07 11:57:18',118,'EXECUTED','3:5d084fef99fd1a16393cc7d816ed204a','Create Index','',NULL,'2.0.5'),('1397229912672-3','raza (generated)','20140410pub-person.groovy','2016-09-07 11:57:18',121,'EXECUTED','3:559f049cdfa425971544e536a69be0eb','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1397229912672-4','raza (generated)','20140410pub-person.groovy','2016-09-07 11:57:19',122,'EXECUTED','3:3d91b85b93a8c4cd3d6dc789bc430a41','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1397229912672-5','raza (generated)','20140410pub-person.groovy','2016-09-07 11:57:19',123,'EXECUTED','3:08c1f76ea58fa85921bf32094c94f601','Create Index','',NULL,'2.0.5'),('1397229912672-6','raza (generated)','20140410pub-person.groovy','2016-09-07 11:57:19',124,'EXECUTED','3:d81a3cf8716a8f5d34f2cf1bd23f910c','Drop Table','',NULL,'2.0.5'),('1397230578681-1','raza (generated)','20140410createPubPerson.groovy','2016-09-07 11:57:19',125,'EXECUTED','3:0c5fbc951d381cd1dfd2e8ee12de45d0','Create Table','',NULL,'2.0.5'),('1397230578681-4','raza (generated)','20140410createPubPerson.groovy','2016-09-07 11:57:19',126,'EXECUTED','3:96631b1163f1d06195275a207fe829f5','Add Primary Key','',NULL,'2.0.5'),('1397230578681-5','raza (generated)','20140410createPubPerson.groovy','2016-09-07 11:57:19',129,'EXECUTED','3:9e05d6bb8bf362dcab56f5f06e6f2aa5','Add Foreign Key Constraint','',NULL,'2.0.5'),('1397230578681-6','raza (generated)','20140410createPubPerson.groovy','2016-09-07 11:57:19',130,'EXECUTED','3:7e3933f342f012e0dd39a4fe119f099f','Add Foreign Key Constraint','',NULL,'2.0.5'),('1397230578681-7','raza (generated)','20140410createPubPerson.groovy','2016-09-07 11:57:19',127,'EXECUTED','3:eafb4fe16bb9608ffd554b0b024677d6','Create Index','',NULL,'2.0.5'),('1397230578681-8','raza (generated)','20140410createPubPerson.groovy','2016-09-07 11:57:19',128,'EXECUTED','3:75cb2c483acc4c1cd383506d479311d8','Create Index','',NULL,'2.0.5'),('1403411311764-1','mglont (generated)','20140622_add_model_submission_and_publication_id.groovy','2016-09-07 11:57:19',134,'EXECUTED','3:72fad2795490291c5b0f73fa062b5870','Add Column','',NULL,'2.0.5'),('1403411311764-2','mglont (generated)','20140622_add_model_submission_and_publication_id.groovy','2016-09-07 11:57:19',135,'EXECUTED','3:15103ff1d27617c1af90618eef4808eb','Add Column','',NULL,'2.0.5'),('1403699898993-1','mglont (generated)','20140625make_perennial_id_unique.groovy','2016-09-07 11:57:19',137,'EXECUTED','3:44ae9376f0fcb7afbd0484b31e9da57d','Create Index','',NULL,'2.0.5'),('1404931436939-1','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',138,'EXECUTED','3:4bd0f3cde448127c2c561d440afb08f2','Create Table','',NULL,'2.0.5'),('1404931436939-10','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',144,'EXECUTED','3:71a268418fab45a38c12c70355f1779b','Create Index','',NULL,'2.0.5'),('1404931436939-2','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',139,'EXECUTED','3:a80635b5e0d6981c5f9a0f71e3bbe520','Create Table','',NULL,'2.0.5'),('1404931436939-3','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',140,'EXECUTED','3:5db21a2f0d90bbeeab3b917496c1fc3f','Add Primary Key','',NULL,'2.0.5'),('1404931436939-4','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',145,'EXECUTED','3:38a499d463dbc7b07dc3a1df66eebfbb','Add Foreign Key Constraint','',NULL,'2.0.5'),('1404931436939-5','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',146,'EXECUTED','3:f1975dcb653bb2e840550bff0e65a9f1','Add Foreign Key Constraint','',NULL,'2.0.5'),('1404931436939-6','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',147,'EXECUTED','3:89886ac19145e8b67c84ce11bdce4787','Add Foreign Key Constraint','',NULL,'2.0.5'),('1404931436939-7','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',141,'EXECUTED','3:4c68c16ee1e0c6fd6ea8ebfdbcae8c00','Create Index','',NULL,'2.0.5'),('1404931436939-8','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',142,'EXECUTED','3:2f58d91ca91b1c7a21f8b89fdfc4e152','Create Index','',NULL,'2.0.5'),('1404931436939-9','mglont (generated)','20140708users_have_teams.groovy','2016-09-07 11:57:19',143,'EXECUTED','3:84b0c1d470ae994b7ede696ee4960279','Create Index','',NULL,'2.0.5'),('1413301526455-1','raza (generated)','notifications.groovy','2016-09-07 11:57:20',150,'EXECUTED','3:f144cc9b3d3b7f26b9a1c075ed960980','Create Table','',NULL,'2.0.5'),('1413301526455-10','raza (generated)','notifications.groovy','2016-09-07 11:57:20',156,'EXECUTED','3:a10c34d7af526ee7142548d4296f0f0c','Create Index','',NULL,'2.0.5'),('1413301526455-11','raza (generated)','notifications.groovy','2016-09-07 11:57:20',157,'EXECUTED','3:45678c1e9d4755aad7eb0d3e427231a7','Create Index','',NULL,'2.0.5'),('1413301526455-12','raza (generated)','notifications.groovy','2016-09-07 11:57:20',158,'EXECUTED','3:ebd2f2c77db5d5074433e1ab1ad878a0','Create Index','',NULL,'2.0.5'),('1413301526455-2','raza (generated)','notifications.groovy','2016-09-07 11:57:20',151,'EXECUTED','3:aa74fdfc46a47724179b8731cbe775cc','Create Table','',NULL,'2.0.5'),('1413301526455-3','raza (generated)','notifications.groovy','2016-09-07 11:57:20',152,'EXECUTED','3:29aff7fd6c7b0ce84da47a70313e2366','Create Table','',NULL,'2.0.5'),('1413301526455-4','raza (generated)','notifications.groovy','2016-09-07 11:57:20',153,'EXECUTED','3:70b1ec536ab3d85917f6e0ca3f42f585','Modify data type','',NULL,'2.0.5'),('1413301526455-5','raza (generated)','notifications.groovy','2016-09-07 11:57:20',154,'EXECUTED','3:1b408a386766d9c20b0f9e1ed9566cc0','Add Not-Null Constraint','',NULL,'2.0.5'),('1413301526455-6','raza (generated)','notifications.groovy','2016-09-07 11:57:20',159,'EXECUTED','3:26955a55bf8b3cfbeb9e3d74a5dda3f9','Add Foreign Key Constraint','',NULL,'2.0.5'),('1413301526455-7','raza (generated)','notifications.groovy','2016-09-07 11:57:20',160,'EXECUTED','3:f09e1f524603fa9b59281d0b3ba07cd6','Add Foreign Key Constraint','',NULL,'2.0.5'),('1413301526455-8','raza (generated)','notifications.groovy','2016-09-07 11:57:20',161,'EXECUTED','3:bf64905e739b2ecb31db06258e53c553','Add Foreign Key Constraint','',NULL,'2.0.5'),('1413301526455-9','raza (generated)','notifications.groovy','2016-09-07 11:57:20',155,'EXECUTED','3:453b01a31d1452849d1368f02a335210','Create Index','',NULL,'2.0.5'),('1413371021073-1','raza (generated)','addSenderToNotification.groovy','2016-09-07 11:57:20',162,'EXECUTED','3:e9a8a5586547dceb72e41bd5c832147c','Add Column','',NULL,'2.0.5'),('1413371021073-2','raza (generated)','addSenderToNotification.groovy','2016-09-07 11:57:20',164,'EXECUTED','3:042aa31e41c00a8834cffaba004f3dfe','Add Foreign Key Constraint','',NULL,'2.0.5'),('1413371021073-3','raza (generated)','addSenderToNotification.groovy','2016-09-07 11:57:20',163,'EXECUTED','3:fffc336f37793e86b7df3611e50ba38e','Create Index','',NULL,'2.0.5'),('1413393516794-2','raza (generated)','trackNotificationSeenByUser.groovy','2016-09-07 11:57:20',165,'EXECUTED','3:20d60304866f5f2bcab9a5d3d0f6eb0e','Add Column','',NULL,'2.0.5'),('1413464250471-1','raza (generated)','removeFromFieldFromNotification.groovy','2016-09-07 11:57:20',166,'EXECUTED','3:efb0db8f953a44251bb567f8dd443f64','Drop Column','',NULL,'2.0.5'),('1428594869002-1','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:20',167,'EXECUTED','3:e93be89fbe48968bac8cdcd418f00e83','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1428594869002-12','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',175,'EXECUTED','3:1290643a7386c4e7c78ecc6efdd1478d','Drop Index','',NULL,'2.0.5'),('1428594869002-13','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',176,'EXECUTED','3:9ec4340e7ab742f3eaee703e2dca3c0b','Drop Index','',NULL,'2.0.5'),('1428594869002-14','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',177,'EXECUTED','3:63f883525c678d7a0576b3ea9cbaa88a','Drop Index','',NULL,'2.0.5'),('1428594869002-15','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',178,'EXECUTED','3:eadc6f408db2ec7dd16628e863f7c8cb','Drop Table','',NULL,'2.0.5'),('1428594869002-16','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',179,'EXECUTED','3:15cf3d178aa5f6b2d5278b1f553a7bdd','Drop Table','',NULL,'2.0.5'),('1428594869002-17','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',180,'EXECUTED','3:daf342e3fde3eec1c703b33b42c1c28f','Drop Table','',NULL,'2.0.5'),('1428594869002-18','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',181,'EXECUTED','3:64a7e3551a12f03b517db7d600f47d60','Drop Table','',NULL,'2.0.5'),('1428594869002-19','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',182,'EXECUTED','3:82dac7a0e2748fffef2d2e934348e671','Drop Table','',NULL,'2.0.5'),('1428594869002-2','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:20',168,'EXECUTED','3:4b2f39b675f614b59c1db07563b9a42f','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1428594869002-20','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',183,'EXECUTED','3:58135ac577c0de807114af9701200ddc','Drop Table','',NULL,'2.0.5'),('1428594869002-3','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:20',169,'EXECUTED','3:9f2589220aa0d558d5457000b143e136','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1428594869002-4','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:20',170,'EXECUTED','3:cf9dcb434b3104402408560aa2da1df6','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1428594869002-5','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:20',171,'EXECUTED','3:c05a43c1f017ef82fcdcc9583f152f99','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1428594869002-6','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:20',172,'EXECUTED','3:f858a8f402f5d19f166cef8629b9f3e1','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1428594869002-7','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',173,'EXECUTED','3:339d89ddf803d50de5508ea3b5cd2d56','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1428594869002-8','mglont (generated)','20150409dropMiriamAnnotationTables.groovy','2016-09-07 11:57:21',174,'EXECUTED','3:606c81a16a0f5eef41735df6cee68e96','Drop Foreign Key Constraint','',NULL,'2.0.5'),('1432312720111-1','mglont (generated)','20150522modelFirstPublishedDate.groovy','2016-09-07 11:57:21',184,'EXECUTED','3:1fee70730af4c5df6fc9671c6ce5b45f','Add Column','',NULL,'2.0.5'),('1433757856028-1','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',195,'EXECUTED','3:64ef2f7d159064c40e682ddcd3891e0b','Create Table','',NULL,'2.0.5'),('1433757856028-19','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',201,'EXECUTED','3:057fa5301566214bff29f84dce7f6ece','Add Foreign Key Constraint','',NULL,'2.0.5'),('1433757856028-2','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',196,'EXECUTED','3:6a3a3bb1923f2b14798630cb3be94039','Create Table','',NULL,'2.0.5'),('1433757856028-20','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',202,'EXECUTED','3:82e9256b279d168850033aac6f2b1e1b','Add Foreign Key Constraint','',NULL,'2.0.5'),('1433757856028-21','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',192,'EXECUTED','3:70852074721f8dd885aa99515ad10628','Add Foreign Key Constraint','',NULL,'2.0.5'),('1433757856028-22','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',193,'EXECUTED','3:a0bfaa8401fd53410865ccd25b0234d2','Add Foreign Key Constraint','',NULL,'2.0.5'),('1433757856028-23','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',194,'EXECUTED','3:806df15e2db689f4a032c5ce1bed4e5a','Add Foreign Key Constraint','',NULL,'2.0.5'),('1433757856028-24','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',203,'EXECUTED','3:a11869adeb57770c91fa659a9f577d71','Add Foreign Key Constraint','',NULL,'2.0.5'),('1433757856028-25','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:22',204,'EXECUTED','3:51601dfcc3dd129bef5b459688b4c13d','Add Foreign Key Constraint','',NULL,'2.0.5'),('1433757856028-29','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',198,'EXECUTED','3:e3c5bc5115c3f73616e035f7c446280e','Create Index','',NULL,'2.0.5'),('1433757856028-3','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',186,'EXECUTED','3:e7b5b78f02a36c46d8514b747dec9a94','Create Table','',NULL,'2.0.5'),('1433757856028-30','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',199,'EXECUTED','3:ce780dd5b3aea8c4dcdf392dabeebfc6','Create Index','',NULL,'2.0.5'),('1433757856028-31','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',189,'EXECUTED','3:0c72da637c874b65bc41eba2624f1677','Create Index','',NULL,'2.0.5'),('1433757856028-32','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',190,'EXECUTED','3:912bf053e338eaa5a704ddc9242fa272','Create Index','',NULL,'2.0.5'),('1433757856028-33','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',191,'EXECUTED','3:f467cf167db683423102ddbfe42b3b4a','Create Index','',NULL,'2.0.5'),('1433757856028-34','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',200,'EXECUTED','3:6ff208aca564f85f3207cf9773a2d28e','Create Index','',NULL,'2.0.5'),('1433757856028-4','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',187,'EXECUTED','3:69c9b6475ab65606bd356fcbe68de1bd','Create Table','',NULL,'2.0.5'),('1433757856028-5','mglont (generated)','20150608_resourceReference.groovy','2016-09-07 11:57:21',188,'EXECUTED','3:bd397efb270df13ee89c1e87b6392724','Create Table','',NULL,'2.0.5'),('1433757856028-6','mglont (generated)','20150608_elementAnnotations.groovy','2016-09-07 11:57:21',197,'EXECUTED','3:67913ae58badb2fb576881c63adf9081','Create Table','',NULL,'2.0.5'),('1433776637034-1','mglont (generated)','20150608_make_wcm_space_alias_uri_nullable.groovy','2016-09-07 11:57:22',205,'EXECUTED','3:d48543c63e0c58c0d4dfbb4d0c1c9259','Drop Not-Null Constraint','',NULL,'2.0.5'),('1435276170713-1','mglont (generated)','20150626_addCollectionNameToResourceReference.groovy','2016-09-07 11:57:22',209,'EXECUTED','3:769b44a3ea00948f45b6f99259888203','Add Column','',NULL,'2.0.5'),('1444642187148-1','sarala (generated)','20151012_addMetadataValidationToRevision.groovy','2016-09-07 11:57:22',210,'EXECUTED','3:462c642a879181f653f2021506a3de08','Add Column','',NULL,'2.0.5'),('1444642187148-2','sarala (generated)','20151012_addMetadataValidationToRevision.groovy','2016-09-07 11:57:22',211,'EXECUTED','3:bb97b1dd0f2a0fb688328ff79407178a','Add Column','',NULL,'2.0.5'),('1449476671923-1','mglont (generated)','20151207_nullable_xref_uri.groovy','2016-09-07 11:57:22',213,'EXECUTED','3:2c5b5a8e9bc523416226c4e98653774a','Drop Not-Null Constraint','',NULL,'2.0.5'),('1468405552304-1','tung (generated)','20160713_createTableQcInfo.groovy','2016-09-07 11:57:22',217,'EXECUTED','3:f099e94eee434979db80e7e7f149d4b3','Create Table','',NULL,'2.0.5'),('1468405552304-2','sarala (generated)','20160718_addCertificationConstraints.groovy','2016-09-07 11:57:22',219,'EXECUTED','3:60fd9909868b0a4d8fc2cc7942e60903','Drop Not-Null Constraint','',NULL,'2.0.5'),('1468405552304-3','sarala (generated)','20160718_addCertificationConstraints.groovy','2016-09-07 11:57:22',220,'EXECUTED','3:a9520d80844454aa2064ac5d85c6deb1','Create Index','',NULL,'2.0.5'),('1468405552304-4','sarala (generated)','20160718_addCertificationConstraints.groovy','2016-09-07 11:57:22',221,'EXECUTED','3:5710896b19543b85f088070b6555fa9b','Add Foreign Key Constraint','',NULL,'2.0.5'),('1633776637124-1','raza','20150610-DropResourceReferenceConstraints.groovy','2016-09-07 11:57:22',206,'EXECUTED','3:866f27c8ad173d57e8d4362c8d0a6f50','Drop Not-Null Constraint','',NULL,'2.0.5'),('1633776637124-2','raza','20150610-DropResourceReferenceConstraints.groovy','2016-09-07 11:57:22',207,'EXECUTED','3:37e3fbe9439336b32a5694c65e21c639','Drop Not-Null Constraint','',NULL,'2.0.5'),('20150611-1','Mihai Glont','20150611-widenResourceReferenceName.groovy','2016-09-07 11:57:22',208,'EXECUTED','3:784f486ee47c11a5418573f808765f99','Modify data type','',NULL,'2.0.5'),('20160114-162125','Tung Nguyen','20160114-makeResourceReferenceNameText.groovy','2016-09-07 11:57:22',214,'EXECUTED','3:a486e00247c7337f5bc536eaa54515b2','Modify data type','',NULL,'2.0.5'),('20160229-131102-1','Tung Nguyen','20160229-updateModelFormatName-Unknown2Original.groovy','2016-09-07 11:57:22',215,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('add-person_id-to-users-20140223-1','Mihai Glont','author2person.groovy','2016-09-07 11:57:18',97,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('addQcInfoToRevision_20160718_172011','tung (generated)','20160718_addQcInfoToRevision.groovy','2016-09-07 11:57:22',218,'EXECUTED','3:3dcc19d281a7333103653b906dad2dca','Add Column','',NULL,'2.0.5'),('delete persons 1','raza and mihai','20140410createPubPerson.groovy','2016-09-07 11:57:19',131,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('import non pubmed authors manually','raza and mihai','20140410createPubPerson.groovy','2016-09-07 11:57:19',133,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('import pubmed authors 2','raza and mihai','20140410createPubPerson.groovy','2016-09-07 11:57:19',132,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('improve-rdf-file-description-20160510','mglont','20160511_updateRDFdescription.groovy','2016-09-07 11:57:22',216,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('make-authors-and-users-persons-20140223-1','Mihai Glont','author2person.groovy','2016-09-07 11:57:17',93,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('make-repository-file-paths-relative-20140804','Mihai Glont','20140804_make_repoFile_path_relative.groovy','2016-09-07 11:57:20',149,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('make-vcsIdentifier-paths-relative-20140801','Mihai Glont','20140801_make_vcsIdentifier_paths_relative.groovy','2016-09-07 11:57:20',148,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('populate-revision-validation-level','Mihai Glont','20151105_populate_revision_validation_level.groovy','2016-09-07 11:57:22',212,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('populate-submission-id-20140624','Mihai Glont','20140623populate_model_submission_and_publication_id.groovy','2016-09-07 11:57:19',136,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('publication_author-becomes-publication_person-20140223-1','Mihai Glont','author2person.groovy','2016-09-07 11:57:18',95,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5'),('record-date-model-was-published-20150522-1','Mihai Glont','20150522modelFirstPublishedDate.groovy','2016-09-07 11:57:21',185,'EXECUTED','3:c31ab3aa91c017799bb9b50f4bef36d2','Grails Change','',NULL,'2.0.5');
/*!40000 ALTER TABLE `DATABASECHANGELOG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DATABASECHANGELOGLOCK`
--

DROP TABLE IF EXISTS `DATABASECHANGELOGLOCK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATABASECHANGELOGLOCK` (
  `ID` int(11) NOT NULL,
  `LOCKED` tinyint(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOGLOCK`
--

LOCK TABLES `DATABASECHANGELOGLOCK` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOGLOCK` VALUES (1,0,NULL,NULL);
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_class`
--

DROP TABLE IF EXISTS `acl_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_class` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_class`
--

LOCK TABLES `acl_class` WRITE;
/*!40000 ALTER TABLE `acl_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_entry`
--

DROP TABLE IF EXISTS `acl_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_entry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ace_order` int(11) NOT NULL,
  `acl_object_identity` bigint(20) NOT NULL,
  `audit_failure` bit(1) NOT NULL,
  `audit_success` bit(1) NOT NULL,
  `granting` bit(1) NOT NULL,
  `mask` int(11) NOT NULL,
  `sid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acl_object_identity` (`acl_object_identity`,`ace_order`),
  KEY `FK5302D47D8FDB88D5` (`sid`),
  KEY `FK5302D47DB0D9DC4D` (`acl_object_identity`),
  CONSTRAINT `FK5302D47D8FDB88D5` FOREIGN KEY (`sid`) REFERENCES `acl_sid` (`id`),
  CONSTRAINT `FK5302D47DB0D9DC4D` FOREIGN KEY (`acl_object_identity`) REFERENCES `acl_object_identity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_entry`
--

LOCK TABLES `acl_entry` WRITE;
/*!40000 ALTER TABLE `acl_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_object_identity`
--

DROP TABLE IF EXISTS `acl_object_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_object_identity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id_class` bigint(20) NOT NULL,
  `entries_inheriting` bit(1) NOT NULL,
  `object_id_identity` bigint(20) NOT NULL,
  `owner_sid` bigint(20) DEFAULT NULL,
  `parent_object` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `object_id_class` (`object_id_class`,`object_id_identity`),
  KEY `FK2A2BB009A50290B8` (`parent_object`),
  KEY `FK2A2BB00970422CC5` (`object_id_class`),
  KEY `FK2A2BB00990EC1949` (`owner_sid`),
  CONSTRAINT `FK2A2BB00970422CC5` FOREIGN KEY (`object_id_class`) REFERENCES `acl_class` (`id`),
  CONSTRAINT `FK2A2BB00990EC1949` FOREIGN KEY (`owner_sid`) REFERENCES `acl_sid` (`id`),
  CONSTRAINT `FK2A2BB009A50290B8` FOREIGN KEY (`parent_object`) REFERENCES `acl_object_identity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_object_identity`
--

LOCK TABLES `acl_object_identity` WRITE;
/*!40000 ALTER TABLE `acl_object_identity` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_object_identity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_sid`
--

DROP TABLE IF EXISTS `acl_sid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_sid` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` bit(1) NOT NULL,
  `sid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sid` (`sid`,`principal`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_sid`
--

LOCK TABLES `acl_sid` WRITE;
/*!40000 ALTER TABLE `acl_sid` DISABLE KEYS */;
INSERT INTO `acl_sid` VALUES (1,'','administrator');
/*!40000 ALTER TABLE `acl_sid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `element_annotation`
--

DROP TABLE IF EXISTS `element_annotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `element_annotation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `creator_id` varchar(255) NOT NULL,
  `revision_id` bigint(20) NOT NULL,
  `statement_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCFAB2C52265115F5` (`revision_id`),
  KEY `FKCFAB2C5253C160C8` (`statement_id`),
  CONSTRAINT `FKCFAB2C52265115F5` FOREIGN KEY (`revision_id`) REFERENCES `revision` (`id`),
  CONSTRAINT `FKCFAB2C5253C160C8` FOREIGN KEY (`statement_id`) REFERENCES `statement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `element_annotation`
--

LOCK TABLES `element_annotation` WRITE;
/*!40000 ALTER TABLE `element_annotation` DISABLE KEYS */;
/*!40000 ALTER TABLE `element_annotation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `first_published` datetime DEFAULT NULL,
  `publication_id` bigint(20) DEFAULT NULL,
  `perennialPublicationIdentifier` varchar(255) DEFAULT NULL,
  `submission_id` varchar(255) NOT NULL,
  `vcs_identifier` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `submission_id` (`submission_id`),
  UNIQUE KEY `vcs_identifier` (`vcs_identifier`),
  KEY `FK633FB297C6F423F` (`publication_id`),
  CONSTRAINT `FK633FB297C6F423F` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model`
--

LOCK TABLES `model` WRITE;
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
/*!40000 ALTER TABLE `model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_audit`
--

DROP TABLE IF EXISTS `model_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_audit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `changes_made` longtext,
  `date_created` datetime NOT NULL,
  `format` int(11) NOT NULL,
  `model_id` bigint(20) NOT NULL,
  `success` bit(1) NOT NULL,
  `type` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK309537452C00D884` (`user_id`),
  KEY `FK30953745ADCECDF` (`model_id`),
  CONSTRAINT `FK309537452C00D884` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK30953745ADCECDF` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_audit`
--

LOCK TABLES `model_audit` WRITE;
/*!40000 ALTER TABLE `model_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_format`
--

DROP TABLE IF EXISTS `model_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_format` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `format_version` varchar(255) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identifier` (`identifier`,`format_version`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_format`
--

LOCK TABLES `model_format` WRITE;
/*!40000 ALTER TABLE `model_format` DISABLE KEYS */;
INSERT INTO `model_format` VALUES (1,0,'*','SBML','SBML'),(2,0,'L1V1','SBML','SBML'),(3,0,'L1V2','SBML','SBML'),(4,0,'L2V1','SBML','SBML'),(5,0,'L2V2','SBML','SBML'),(6,0,'L2V3','SBML','SBML'),(7,0,'L2V4','SBML','SBML'),(8,0,'L3V1','SBML','SBML'),(9,0,'*','OMEX','COMBINE archive'),(10,0,'0.1','OMEX','COMBINE archive'),(11,0,'*','PharmML','PharmML'),(12,0,'0.1','PharmML','PharmML'),(13,0,'0.2.1','PharmML','PharmML'),(14,0,'0.3','PharmML','PharmML'),(15,0,'0.3.1','PharmML','PharmML'),(16,0,'0.6','PharmML','PharmML'),(17,0,'0.6.1','PharmML','PharmML'),(18,0,'*','MDL','MDL'),(19,0,'5.0.8','MDL','MDL'),(20,0,'7.0','MDL','MDL'),(21,0,'*','UNKNOWN','Original code'),(22,0,'*','PharmML08','PharmML 0.8.x'),(23,0,'0.8.1','PharmML08','PharmML 0.8.x');
/*!40000 ALTER TABLE `model_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_history_item`
--

DROP TABLE IF EXISTS `model_history_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_history_item` (
  `model_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `last_accessed_date` bigint(20) NOT NULL,
  PRIMARY KEY (`model_id`,`user_id`),
  KEY `FK9388A5342C00D884` (`user_id`),
  KEY `FK9388A534ADCECDF` (`model_id`),
  CONSTRAINT `FK9388A5342C00D884` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK9388A534ADCECDF` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_history_item`
--

LOCK TABLES `model_history_item` WRITE;
/*!40000 ALTER TABLE `model_history_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_history_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `body` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `notification_type` int(11) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK237A88EB36B119DA` (`sender_id`),
  CONSTRAINT `FK237A88EB36B119DA` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_type_preferences`
--

DROP TABLE IF EXISTS `notification_type_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_type_preferences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `notification_type` int(11) NOT NULL,
  `send_mail` bit(1) NOT NULL,
  `send_notification` bit(1) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCBE932472C00D884` (`user_id`),
  CONSTRAINT `FKCBE932472C00D884` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_type_preferences`
--

LOCK TABLES `notification_type_preferences` WRITE;
/*!40000 ALTER TABLE `notification_type_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification_type_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_user`
--

DROP TABLE IF EXISTS `notification_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `notification_id` bigint(20) NOT NULL,
  `notification_seen` bit(1) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA24EF69F1F3D24E5` (`notification_id`),
  KEY `FKA24EF69F2C00D884` (`user_id`),
  CONSTRAINT `FKA24EF69F1F3D24E5` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`),
  CONSTRAINT `FKA24EF69F2C00D884` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_user`
--

LOCK TABLES `notification_user` WRITE;
/*!40000 ALTER TABLE `notification_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `institution` varchar(255) DEFAULT NULL,
  `orcid` varchar(255) DEFAULT NULL,
  `user_real_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orcid` (`orcid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,0,NULL,NULL,'administrator');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferences`
--

DROP TABLE IF EXISTS `preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `num_results` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK769ADEF82C00D884` (`user_id`),
  CONSTRAINT `FK769ADEF82C00D884` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferences`
--

LOCK TABLES `preferences` WRITE;
/*!40000 ALTER TABLE `preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publication`
--

DROP TABLE IF EXISTS `publication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `affiliation` varchar(255) NOT NULL,
  `day` int(11) DEFAULT NULL,
  `issue` int(11) DEFAULT NULL,
  `journal` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `link_provider_id` bigint(20) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `pages` varchar(255) DEFAULT NULL,
  `synopsis` varchar(5000) NOT NULL,
  `title` varchar(255) NOT NULL,
  `volume` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKBFBBA22C46318640` (`link_provider_id`),
  CONSTRAINT `FKBFBBA22C46318640` FOREIGN KEY (`link_provider_id`) REFERENCES `publication_link_provider` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publication`
--

LOCK TABLES `publication` WRITE;
/*!40000 ALTER TABLE `publication` DISABLE KEYS */;
/*!40000 ALTER TABLE `publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publication_link_provider`
--

DROP TABLE IF EXISTS `publication_link_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_link_provider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `identifiers_prefix` varchar(255) DEFAULT NULL,
  `link_type` varchar(255) NOT NULL,
  `pattern` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publication_link_provider`
--

LOCK TABLES `publication_link_provider` WRITE;
/*!40000 ALTER TABLE `publication_link_provider` DISABLE KEYS */;
INSERT INTO `publication_link_provider` VALUES (1,0,'http://identifiers.org/pubmed/','PUBMED','^\\d+'),(2,0,'http://identifiers.org/doi/','DOI','^(doi\\:)?\\d{2}\\.\\d{4}.*'),(3,0,NULL,'CUSTOM','^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]'),(4,0,NULL,'MANUAL_ENTRY','\\A\\z');
/*!40000 ALTER TABLE `publication_link_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publication_person`
--

DROP TABLE IF EXISTS `publication_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publication_person` (
  `publication_id` bigint(20) NOT NULL,
  `person_id` bigint(20) NOT NULL,
  `position` int(11) NOT NULL,
  `pub_alias` varchar(255) NOT NULL,
  PRIMARY KEY (`publication_id`,`person_id`),
  KEY `FKE7516CC87C6F423F` (`publication_id`),
  KEY `FKE7516CC8448831C4` (`person_id`),
  CONSTRAINT `FKE7516CC8448831C4` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FKE7516CC87C6F423F` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publication_person`
--

LOCK TABLES `publication_person` WRITE;
/*!40000 ALTER TABLE `publication_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `publication_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qc_info`
--

DROP TABLE IF EXISTS `qc_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qc_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qc_info`
--

LOCK TABLES `qc_info` WRITE;
/*!40000 ALTER TABLE `qc_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `qc_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualifier`
--

DROP TABLE IF EXISTS `qualifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qualifier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `accession` varchar(255) DEFAULT NULL,
  `namespace` varchar(255) DEFAULT NULL,
  `qualifier_type` varchar(255) NOT NULL,
  `uri` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualifier`
--

LOCK TABLES `qualifier` WRITE;
/*!40000 ALTER TABLE `qualifier` DISABLE KEYS */;
/*!40000 ALTER TABLE `qualifier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repository_file`
--

DROP TABLE IF EXISTS `repository_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repository_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `hidden` bit(1) NOT NULL,
  `main_file` bit(1) NOT NULL,
  `mime_type` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `revision_id` bigint(20) NOT NULL,
  `user_submitted` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK807B5151265115F5` (`revision_id`),
  CONSTRAINT `FK807B5151265115F5` FOREIGN KEY (`revision_id`) REFERENCES `revision` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repository_file`
--

LOCK TABLES `repository_file` WRITE;
/*!40000 ALTER TABLE `repository_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `repository_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_reference`
--

DROP TABLE IF EXISTS `resource_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_reference` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `accession` varchar(255) DEFAULT NULL,
  `collection_name` varchar(255) DEFAULT NULL,
  `datatype` varchar(255) NOT NULL,
  `description` longtext,
  `name` longtext,
  `short_name` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_reference`
--

LOCK TABLES `resource_reference` WRITE;
/*!40000 ALTER TABLE `resource_reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_reference_resource_reference`
--

DROP TABLE IF EXISTS `resource_reference_resource_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_reference_resource_reference` (
  `resource_reference_parents_id` bigint(20) DEFAULT NULL,
  `resource_reference_id` bigint(20) DEFAULT NULL,
  KEY `FK6C1245F758A2D6B` (`resource_reference_id`),
  KEY `FK6C1245F29AEA761` (`resource_reference_parents_id`),
  CONSTRAINT `FK6C1245F29AEA761` FOREIGN KEY (`resource_reference_parents_id`) REFERENCES `resource_reference` (`id`),
  CONSTRAINT `FK6C1245F758A2D6B` FOREIGN KEY (`resource_reference_id`) REFERENCES `resource_reference` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_reference_resource_reference`
--

LOCK TABLES `resource_reference_resource_reference` WRITE;
/*!40000 ALTER TABLE `resource_reference_resource_reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_reference_resource_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_reference_synonyms`
--

DROP TABLE IF EXISTS `resource_reference_synonyms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_reference_synonyms` (
  `resource_reference_id` bigint(20) DEFAULT NULL,
  `synonyms_string` varchar(255) DEFAULT NULL,
  KEY `FK5FECF61D758A2D6B` (`resource_reference_id`),
  CONSTRAINT `FK5FECF61D758A2D6B` FOREIGN KEY (`resource_reference_id`) REFERENCES `resource_reference` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_reference_synonyms`
--

LOCK TABLES `resource_reference_synonyms` WRITE;
/*!40000 ALTER TABLE `resource_reference_synonyms` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_reference_synonyms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revision`
--

DROP TABLE IF EXISTS `revision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revision` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `description` longtext,
  `format_id` bigint(20) NOT NULL,
  `minor_revision` bit(1) NOT NULL,
  `model_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owner_id` bigint(20) NOT NULL,
  `qc_info_id` bigint(20) DEFAULT NULL,
  `revision_number` int(11) NOT NULL,
  `state` varchar(255) NOT NULL,
  `upload_date` datetime NOT NULL,
  `validated` bit(1) NOT NULL,
  `validation_level` varchar(255) NOT NULL,
  `validation_report` varchar(1000) DEFAULT NULL,
  `vcs_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `model_id` (`model_id`,`revision_number`),
  UNIQUE KEY `model_id_2` (`model_id`,`vcs_id`),
  KEY `FKF074B7DB33F8EE8` (`format_id`),
  KEY `FKF074B7DB9D712777` (`qc_info_id`),
  KEY `FKF074B7DB97E7889C` (`owner_id`),
  KEY `FKF074B7DBADCECDF` (`model_id`),
  CONSTRAINT `FKF074B7DB33F8EE8` FOREIGN KEY (`format_id`) REFERENCES `model_format` (`id`),
  CONSTRAINT `FKF074B7DB97E7889C` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKF074B7DB9D712777` FOREIGN KEY (`qc_info_id`) REFERENCES `qc_info` (`id`),
  CONSTRAINT `FKF074B7DBADCECDF` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revision`
--

LOCK TABLES `revision` WRITE;
/*!40000 ALTER TABLE `revision` DISABLE KEYS */;
/*!40000 ALTER TABLE `revision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `authority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authority` (`authority`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,0,'ROLE_USER'),(2,0,'ROLE_CURATOR'),(3,0,'ROLE_ADMIN'),(4,0,'ROLE_QC_PROVIDER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statement`
--

DROP TABLE IF EXISTS `statement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `qualifier_id` bigint(20) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK83B7296FED61A866` (`object_id`),
  KEY `FK83B7296F9C92D2E8` (`qualifier_id`),
  CONSTRAINT `FK83B7296F9C92D2E8` FOREIGN KEY (`qualifier_id`) REFERENCES `qualifier` (`id`),
  CONSTRAINT `FK83B7296FED61A866` FOREIGN KEY (`object_id`) REFERENCES `resource_reference` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statement`
--

LOCK TABLES `statement` WRITE;
/*!40000 ALTER TABLE `statement` DISABLE KEYS */;
/*!40000 ALTER TABLE `statement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_links`
--

DROP TABLE IF EXISTS `tag_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `tag_ref` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7C35D6D45A3B441D` (`tag_id`),
  CONSTRAINT `FK7C35D6D45A3B441D` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_links`
--

LOCK TABLES `tag_links` WRITE;
/*!40000 ALTER TABLE `tag_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `owner_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id` (`owner_id`,`name`),
  KEY `FK36425D97E7889C` (`owner_id`),
  CONSTRAINT `FK36425D97E7889C` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `account_expired` bit(1) NOT NULL,
  `account_locked` bit(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_expired` bit(1) NOT NULL,
  `password_forgotten_code` varchar(255) DEFAULT NULL,
  `password_forgotten_invalidation` datetime DEFAULT NULL,
  `person_id` bigint(20) NOT NULL,
  `registration_code` varchar(255) DEFAULT NULL,
  `registration_invalidation` datetime DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  KEY `FK36EBCB448831C4` (`person_id`),
  CONSTRAINT `FK36EBCB448831C4` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,0,'\0','\0','user@test.com','','4194d1706ed1f408d5e02d672777019f4d5385c766a8c6ca8acba3167d36a7b9','\0',NULL,NULL,1,NULL,NULL,'administrator');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `FK143BF46A86D614A4` (`role_id`),
  KEY `FK143BF46A2C00D884` (`user_id`),
  CONSTRAINT `FK143BF46A2C00D884` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK143BF46A86D614A4` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1),(3,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_team`
--

DROP TABLE IF EXISTS `user_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_team` (
  `team_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`team_id`,`user_id`),
  KEY `FK143CB6512C00D884` (`user_id`),
  KEY `FK143CB651DEFB7744` (`team_id`),
  CONSTRAINT `FK143CB6512C00D884` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK143CB651DEFB7744` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_team`
--

LOCK TABLES `user_team` WRITE;
/*!40000 ALTER TABLE `user_team` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wcm_content`
--

DROP TABLE IF EXISTS `wcm_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcm_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `aliasuri` varchar(50) NOT NULL,
  `changed_by` varchar(255) DEFAULT NULL,
  `changed_on` datetime DEFAULT NULL,
  `content_dependencies` varchar(500) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `identifier` varchar(80) DEFAULT NULL,
  `language` varchar(3) DEFAULT NULL,
  `meta_copyright` varchar(200) DEFAULT NULL,
  `meta_creator` varchar(80) DEFAULT NULL,
  `meta_publisher` varchar(80) DEFAULT NULL,
  `meta_source` varchar(80) DEFAULT NULL,
  `order_index` int(11) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `publish_from` datetime DEFAULT NULL,
  `publish_until` datetime DEFAULT NULL,
  `space_id` bigint(20) NOT NULL,
  `status_id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `valid_for` int(11) DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  `target_id` bigint(20) DEFAULT NULL,
  `content` longtext,
  `file_mime_type` varchar(255) DEFAULT NULL,
  `file_size` bigint(20) DEFAULT NULL,
  `sync_status` smallint(6) DEFAULT NULL,
  `files_count` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `comment_markup` varchar(4) DEFAULT NULL,
  `max_entries_to_display` int(11) DEFAULT NULL,
  `template_id` bigint(20) DEFAULT NULL,
  `allowed_methods` varchar(40) DEFAULT NULL,
  `script_id` bigint(20) DEFAULT NULL,
  `allowgsp` bit(1) DEFAULT NULL,
  `html_title` longtext,
  `keywords` varchar(200) DEFAULT NULL,
  `menu_title` varchar(40) DEFAULT NULL,
  `summary` varchar(500) DEFAULT NULL,
  `user_specific_content` bit(1) DEFAULT NULL,
  `author` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `website_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parent_id` (`parent_id`,`space_id`,`aliasuri`),
  KEY `content_space_Idx` (`space_id`),
  KEY `content_aliasURI_Idx` (`aliasuri`),
  KEY `content_contentName_Idx` (`title`),
  KEY `content_changedOn_Idx` (`changed_on`),
  KEY `content_createdOn_Idx` (`created_on`),
  KEY `FK951FF55BF1D5C51A` (`space_id`),
  KEY `FK951FF55B3A4CE4A4` (`script_id`),
  KEY `FK951FF55B68CFF5A` (`status_id`),
  KEY `FK951FF55B988A1A5A` (`template_id`),
  KEY `FK951FF55B3573F022` (`target_id`),
  KEY `FK951FF55BE1775669` (`parent_id`),
  CONSTRAINT `FK951FF55B3573F022` FOREIGN KEY (`target_id`) REFERENCES `wcm_content` (`id`),
  CONSTRAINT `FK951FF55B3A4CE4A4` FOREIGN KEY (`script_id`) REFERENCES `wcm_content` (`id`),
  CONSTRAINT `FK951FF55B68CFF5A` FOREIGN KEY (`status_id`) REFERENCES `wcm_status` (`id`),
  CONSTRAINT `FK951FF55B988A1A5A` FOREIGN KEY (`template_id`) REFERENCES `wcm_content` (`id`),
  CONSTRAINT `FK951FF55BE1775669` FOREIGN KEY (`parent_id`) REFERENCES `wcm_content` (`id`),
  CONSTRAINT `FK951FF55BF1D5C51A` FOREIGN KEY (`space_id`) REFERENCES `wcm_space` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcm_content`
--

LOCK TABLES `wcm_content` WRITE;
/*!40000 ALTER TABLE `wcm_content` DISABLE KEYS */;
INSERT INTO `wcm_content` VALUES (1,0,'templates','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,'2016-09-07 11:36:35',NULL,1,5,'templates',86400,'org.weceem.content.WcmFolder',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,0,'default-blog-template','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,3,1,'2016-09-07 11:36:35',NULL,1,5,'default-blog-template',86400,'org.weceem.content.WcmTemplate',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<html>\n<head>\n    <wcm:widget path=\"widgets/head\"/>\n    <wcm:ifContentIs type=\"org.weceem.blog.WcmBlog\">  \n        <wcm:feedLink type=\"rss\" path=\"${node.absoluteURI}\"/>  \n        <wcm:feedLink type=\"atom\" path=\"${node.absoluteURI}\"/>  \n    </wcm:ifContentIs>\n</head>\n<body>\n    <wcm:widget path=\"widgets/content-head\"/>\n\n    <div id=\"container\">\n        <wcm:widget path=\"widgets/navigation\"/>\n        <wcm:widget path=\"widgets/branding\"/>\n        <div id=\"contentContainer\">\n            <div id=\"socialMedia\">\n            <!-- TODO: integrate social media button or use area for other part -->\n            </div>\n            <div id=\"content\">\n                <div id=\"main\">\n                  <!-- If the template is on a Blog node, render list of articles -->\n                  <wcm:ifContentIs type=\"org.weceem.blog.WcmBlog\">\n                    <wcm:eachChild type=\"org.weceem.blog.WcmBlogEntry\" var=\"child\" max=\"${node.maxEntriesToDisplay}\" sort=\"publishFrom\" order=\"desc\">\n                      <!-- Render the BlogEntry using the widget -->\n                      <div class=\"blog-entry\">\n                        <h1><wcm:link node=\"${child}\">${child.title.encodeAsHTML()}</wcm:link></h1>\n                        <wcm:widget model=\"[node:child]\" path=\"widgets/blog-entry-widget\"/>\n                      </div>\n                    </wcm:eachChild>\n                  </wcm:ifContentIs>\n                  <!-- If the template is on a BlogEntry node, render just this article -->\n                  <wcm:ifContentIs type=\"org.weceem.blog.WcmBlogEntry\">\n                        <!-- Render the BlogEntry using the widget -->\n                    <h1>${node.title}</h1>\n                    <div class=\"blog-entry\">\n                      <wcm:widget model=\"[node:node]\" path=\"widgets/blog-entry-widget\"/>\n                    </div>\n                    <wcm:ifUserCanEdit>\n                      <g:link controller=\"wcmEditor\" action=\"edit\" id=\"${node.id}\">Edit</g:link>    \n                    </wcm:ifUserCanEdit>\n                  </wcm:ifContentIs>\n                </div>\n                <div id=\"sideBar\">\n                  <wcm:ifContentIs type=\"org.weceem.blog.WcmBlog\">\n                    <div class=\"element\">\n                      <h1>Archive</h1>\n                      <h2></h2>\n                      <p>\n                      <ul>\n                        <wcm:archiveList path=\"${node}\">\n                          <li><a href=\"${link}\">${wcm.monthName(value:month).encodeAsHTML()} ${year}</a></li>\n                        </wcm:archiveList>\n                      </ul>\n                      </p>\n                    </div>\n                  </wcm:ifContentIs>\n                </div>\n            </div>\n            <wcm:widget path=\"widgets/links\"/>\n        </div>\n    </div>\n    <wcm:widget path=\"widgets/footer\"/>\n</body>\n</html>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,NULL,NULL,NULL),(3,0,'widgets','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,NULL,'2016-09-07 11:36:35',NULL,1,5,'widgets',86400,'org.weceem.content.WcmFolder',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,0,'navigation','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,2,3,'2016-09-07 11:36:35',NULL,1,5,'navigation',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div id=\"nav\">\n  <div class=\"left\"><a href=\"${g.createLink(uri: \'/\')}\"><g:message code=\"jummp.main.tabs.about\"/></a></div>\n  <div class=\"right\"><a href=\"#\"><g:message code=\"jummp.main.tabs.search\"/></a></div>\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,0,'head','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,4,3,'2016-09-07 11:36:35',NULL,1,5,'head',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<!-- Render the page title -->\n<title>${node.titleForHTML.encodeAsHTML()}</title>\n<link rel=\"shortcut icon\" href=\"${g.createLink(uri: \'/images/favicon.ico\')}\"/>\n<less:stylesheet name=\"jummp\"/>\n<less:scripts />',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,0,'blog-entry-widget','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,6,3,'2016-09-07 11:36:35',NULL,1,5,'blog-entry-widget',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div class=\"blog-entry-date\"><g:formatDate date=\"${node.publishFrom}\" format=\"dd MMM yyyy \'at\' hh:mm\"/></div>\n<div class=\"blog-entry-content\">\n${node.content}\n</div>\n<div class=\"blog-entry-post-info\">\n    <span class=\"quiet\"><wcm:countChildren node=\"${node}\" type=\"org.weceem.content.WcmComment\"/> Comments</span>\n</div>\n<div class=\"blog-entry-post-info\">\n    <span class=\"quiet\">Tags:\n    <wcm:join in=\"${node.tags}\" delimiter=\", \" var=\"tag\">\n        <wcm:searchLink mode=\'tag\' query=\"${tag}\">${tag.encodeAsHTML()}</wcm:searchLink>\n    </wcm:join>\n    </span>\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,0,'footer','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,8,3,'2016-09-07 11:36:35',NULL,1,5,'footer',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div id=\"footer\">\n  Build: <g:render template=\"/templates/version\"/>\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,0,'news-sidebar-item','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,10,3,'2016-09-07 11:36:35',NULL,1,5,'news-sidebar-item',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div class=\"element\">\n  <g:set var=\"newsBlog\" value=\"${wcm.findNode(path:\'news\')}\"/>\n  <h1>News</h1>\n  <wcm:eachChild path=\"${newsBlog}\" type=\"org.weceem.blog.WcmBlogEntry\" max=\"1\" sort=\"publishFrom\" order=\"desc\">\n    <div class=\"rss\">\n        <a href=\"wcm-tools/feed/rss/jummp/news\">\n            <img src=\"${resource(dir: \'images\', file: \'feed.png\')}\"/>\n        </a>\n    </div>\n    <h2><a href=\"${wcm.createLink(path: it)}\">${it.title.encodeAsHTML()}</a></h2>\n${it.summary}\n  </wcm:eachChild>\n  <g:if test=\"${wcm.countChildren(node: newsBlog) == \'0\'}\">\n    <h2>No News published yet</h2>\n  </g:if>\n  <p>\n    <a href=\"${wcm.createLink(path: \'news\')}\">more</a>\n  </p>\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,0,'admin','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,16,3,'2016-09-07 11:36:35',NULL,1,5,'admin',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div class=\"element\">\n  <h1>Administration</h1>\n  <h2></h2>\n  <ul>\n    <li><g:link controller=\"wcm-admin\">Content Management System</g:link></li>\n    <li><g:link controller=\"configuration\">System Configuration</g:link></li>\n    <li><g:link controller=\"userAdministration\">User Administration</g:link></li>\n    <li><g:link controller=\"miriam\">Miriam Administration</g:link></li>\n  </ul>\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,0,'links','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,18,3,'2016-09-07 11:36:35',NULL,1,5,'links',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div id=\"linkArea\">\nHere are the page links\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,0,'content-head','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,20,3,'2016-09-07 11:36:35',NULL,1,5,'content-head',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div id=\"topBackground\"></div>\n<div id=\"middleBackground\"></div>\n<div id=\"logo\"></div>\n<div id=\"modeSwitch\">\n  <!-- TODO: active class has to be set on really selected mode -->\n  <jummp:button class=\"left active\"><g:message code=\"jummp.main.search\"/></jummp:button>\n  <jummp:button class=\"right\"><g:message code=\"jummp.main.submit\"/></jummp:button>\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,0,'dkfz-feed','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,22,3,'2016-09-07 11:36:35',NULL,1,5,'dkfz-feed',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div class=\"element external\">\n  <h1>Latest DKFZ News</h1>\n  <div class=\"rss\">\n    <a href=\"http://www.dkfz.de/xml/rssfeed/2.0/en/index.php\">\n        <img src=\"${resource(dir: \'images\', file: \'feed.png\')}\"/>\n    </a>\n  </div>\n  <wcm:dataFeed url=\"http://www.dkfz.de/xml/rssfeed/2.0/en/index.php\" max=\"1\" custom=\"true\">\n    <h2><a href=\"${item.link}\" target=\"_blank\">${item.title.encodeAsHTML()}</a></h2>\n    <p>${item.description}</p>\n  </wcm:dataFeed>\n  <p><a href=\"http://www.dkfz.de/en/presse/pressemitteilungen/\" target=\"_blank\">more</a></p>\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,0,'branding','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,24,3,'2016-09-07 11:36:35',NULL,1,5,'branding',86400,'org.weceem.content.WcmWidget',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<div id=\"branding\">\n</div>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,0,'footer','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,4,NULL,'2016-09-07 11:36:35',NULL,1,5,'footer',86400,'org.weceem.html.WcmHTMLContent',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<wcm:widget path=\"widgets/footer\"/>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,0,'links','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,7,NULL,'2016-09-07 11:36:35',NULL,1,5,'links',86400,'org.weceem.html.WcmHTMLContent',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<wcm:widget path=\"widgets/links\"/>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,0,'main-sidebar','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,9,NULL,'2016-09-07 11:36:35',NULL,1,5,'main-sidebar',86400,'org.weceem.html.WcmHTMLContent',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<sec:ifAnyGranted roles=\"ROLE_ADMIN\">\n  <wcm:widget path=\"widgets/admin\"/>\n</sec:ifAnyGranted>\n<wcm:widget path=\"widgets/news-sidebar-item\"/>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,0,'index','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,12,NULL,'2016-09-07 11:36:35',NULL,1,5,'index',86400,'org.weceem.html.WcmHTMLContent',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<h2>\n    Welcome to JUMMP</h2>\n<h3>\n    Just a Model management Platform (Jummp) will be a software infrastructure for the collaborative development and management of biochemical models</h3>\n<p>\n    Here you can put your custom text!</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,0,'branding','unknown','2016-09-07 11:36:35',NULL,'unknown','2016-09-07 11:36:35',NULL,NULL,'eng',NULL,NULL,NULL,NULL,14,NULL,'2016-09-07 11:36:35',NULL,1,5,'branding',86400,'org.weceem.html.WcmHTMLContent',NULL,'<%--\n Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),\n Deutsches Krebsforschungszentrum (DKFZ)\n\n This file is part of Jummp.\n\n Jummp is free software; you can redistribute it and/or modify it under the\n terms of the GNU Affero General Public License as published by the Free\n Software Foundation; either version 3 of the License, or (at your option) any\n later version.\n\n Jummp is distributed in the hope that it will be useful, but WITHOUT ANY\n WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.\n \n You should have received a copy of the GNU Affero General Public License along \n with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.\n--%>\n\n\n\n\n\n\n\n\n\n\n\n<wcm:widget path=\"widgets/branding\"/>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,0,'Submission-To-The-Repository',NULL,NULL,NULL,'administrator','2017-12-19 16:32:03',NULL,NULL,'eng',NULL,NULL,NULL,NULL,15,NULL,'2017-12-19 16:32:03',NULL,1,5,'Submission-To-The-Repository',3600,'org.weceem.html.WcmHTMLContent',NULL,'<h2>\r\n	Submission Terms and Conditions</h2>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Put terms and conditions here...</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `wcm_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wcm_content_version`
--

DROP TABLE IF EXISTS `wcm_content_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcm_content_version` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `content_title` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `object_class_name` varchar(255) NOT NULL,
  `object_content` longtext NOT NULL,
  `object_key` bigint(20) NOT NULL,
  `revision` int(11) DEFAULT NULL,
  `space_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcm_content_version`
--

LOCK TABLES `wcm_content_version` WRITE;
/*!40000 ALTER TABLE `wcm_content_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `wcm_content_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wcm_related_content`
--

DROP TABLE IF EXISTS `wcm_related_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcm_related_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `is_internal` bit(1) NOT NULL,
  `last_checked_on` datetime NOT NULL,
  `relation_type` varchar(255) NOT NULL,
  `source_content_id` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `target_content_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKDE7BDB47361353A8` (`target_content_id`),
  KEY `FKDE7BDB47AD0EC2DE` (`source_content_id`),
  CONSTRAINT `FKDE7BDB47361353A8` FOREIGN KEY (`target_content_id`) REFERENCES `wcm_content` (`id`),
  CONSTRAINT `FKDE7BDB47AD0EC2DE` FOREIGN KEY (`source_content_id`) REFERENCES `wcm_content` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcm_related_content`
--

LOCK TABLES `wcm_related_content` WRITE;
/*!40000 ALTER TABLE `wcm_related_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `wcm_related_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wcm_space`
--

DROP TABLE IF EXISTS `wcm_space`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcm_space` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `aliasuri` varchar(80) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `aliasuri` (`aliasuri`),
  KEY `space_name_Idx` (`name`),
  KEY `space_aliasURI_Idx` (`aliasuri`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcm_space`
--

LOCK TABLES `wcm_space` WRITE;
/*!40000 ALTER TABLE `wcm_space` DISABLE KEYS */;
INSERT INTO `wcm_space` VALUES (1,0,'','Default');
/*!40000 ALTER TABLE `wcm_space` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wcm_status`
--

DROP TABLE IF EXISTS `wcm_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wcm_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `code` int(11) NOT NULL,
  `description` varchar(80) NOT NULL,
  `public_content` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wcm_status`
--

LOCK TABLES `wcm_status` WRITE;
/*!40000 ALTER TABLE `wcm_status` DISABLE KEYS */;
INSERT INTO `wcm_status` VALUES (1,0,100,'draft','\0'),(2,0,150,'unmoderated','\0'),(3,0,200,'reviewed','\0'),(4,0,300,'approved','\0'),(5,0,400,'published',''),(6,0,500,'archived','\0');
/*!40000 ALTER TABLE `wcm_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-19 16:32:59
